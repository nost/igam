package mblog.web.controller.desk.game;

import mblog.core.persist.entity.ElementPo;
import mblog.core.persist.service.ElementService;
import mblog.core.persist.service.GameElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 元素
 * */
@Controller
@RequestMapping("/element")
public class ElementController {
    @Autowired
    ElementService elementService;
    @Autowired
    GameElementService gameElementService;

    /**
     * 搜索元素名
     * */
    @ResponseBody
    @RequestMapping("/search")
    public List<ElementPo> search(String word){
        return elementService.elementPos(word);
    }
}
