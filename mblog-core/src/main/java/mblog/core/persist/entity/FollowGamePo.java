package mblog.core.persist.entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;

/**
 * 对游戏的关注
 * */
@Entity
@Table(name = "mto_follow_game")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FollowGamePo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * 所属用户Id
     */
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserPO user;

    /**
     * 关注游戏的Id
     * */
    @ManyToOne
    @JoinColumn(name = "follow_game_id", nullable = false)
    private GamePo followGame;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserPO getUser() {
        return user;
    }

    public void setUser(UserPO user) {
        this.user = user;
    }

    public GamePo getFollowGame() {
        return followGame;
    }

    public void setFollowGame(GamePo followGame) {
        this.followGame = followGame;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
