/*
Navicat MySQL Data Transfer

Source Server         : my
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : db_mblog

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2018-05-09 17:52:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mto_auth_menu
-- ----------------------------
DROP TABLE IF EXISTS `mto_auth_menu`;
CREATE TABLE `mto_auth_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parent_ids` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_auth_menu
-- ----------------------------
INSERT INTO `mto_auth_menu` VALUES ('1', '根目录', '', '', '1', '/', '1', null);
INSERT INTO `mto_auth_menu` VALUES ('2', '后台管理', null, 'admin', '1', 'admin', '1', null);
INSERT INTO `mto_auth_menu` VALUES ('4', '文章管理', null, 'posts:view', '2', 'admin/posts/list', '2', 'fa fa-clone icon-xlarge');
INSERT INTO `mto_auth_menu` VALUES ('5', '文章修改', null, 'posts:edit', '1', null, '4', null);
INSERT INTO `mto_auth_menu` VALUES ('6', '文章查看', null, 'posts:view', '1', '', '4', null);
INSERT INTO `mto_auth_menu` VALUES ('8', '评论管理', null, 'comments:view', '4', 'admin/comments/list', '2', 'fa fa-comments-o icon-xlarge');
INSERT INTO `mto_auth_menu` VALUES ('9', '删除评论', null, 'comments:edit', '1', null, '8', null);
INSERT INTO `mto_auth_menu` VALUES ('10', '评论查看', null, 'comments:view', '1', '', '8', null);
INSERT INTO `mto_auth_menu` VALUES ('11', '栏目管理', null, 'group:view', '5', 'admin/channel/list', '2', 'fa fa-tags icon-xlarge');
INSERT INTO `mto_auth_menu` VALUES ('12', '删除栏目', null, 'group:edit', '1', null, '11', null);
INSERT INTO `mto_auth_menu` VALUES ('13', '修改栏目', null, 'group:edit', '2', null, '11', null);
INSERT INTO `mto_auth_menu` VALUES ('15', '系统配置', null, 'config:view', '6', 'admin/config/', '2', 'fa fa-sun-o icon-xlarge');
INSERT INTO `mto_auth_menu` VALUES ('16', '修改配置', null, 'config:edit', '1', null, '15', null);
INSERT INTO `mto_auth_menu` VALUES ('17', '用户管理', null, 'users:view', '3', 'admin/users/list', '2', 'fa fa-user icon-xlarge');
INSERT INTO `mto_auth_menu` VALUES ('18', '禁用用户', null, 'users:edit', '1', '', '17', null);
INSERT INTO `mto_auth_menu` VALUES ('19', '修改密码', null, 'users:edit', '1', null, '17', null);
INSERT INTO `mto_auth_menu` VALUES ('20', '用户查看', null, 'users:view', '1', '', '17', null);
INSERT INTO `mto_auth_menu` VALUES ('35', '角色管理', null, 'roles:view', '7', 'admin/roles/list', '2', 'fa fa fa-registered icon-xlarge');
INSERT INTO `mto_auth_menu` VALUES ('36', '角色修改', null, 'roles:edit', '0', null, '35', null);
INSERT INTO `mto_auth_menu` VALUES ('37', '角色查看', null, 'roles:view', '1', '', '35', null);
INSERT INTO `mto_auth_menu` VALUES ('41', '菜单管理', null, 'authMenus:view', '8', 'admin/authMenus/list', '2', 'fa fa-reorder icon-xlarge');
INSERT INTO `mto_auth_menu` VALUES ('42', '菜单修改', null, 'authMenus:edit', '0', '', '41', null);
INSERT INTO `mto_auth_menu` VALUES ('43', '菜单查看', null, 'authMenus:view', '1', '', '41', null);

-- ----------------------------
-- Table structure for mto_channels
-- ----------------------------
DROP TABLE IF EXISTS `mto_channels`;
CREATE TABLE `mto_channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_2s863lts1h6m7c30152262cvj` (`key_`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_channels
-- ----------------------------
INSERT INTO `mto_channels` VALUES ('1', 'games', '游戏', '0');
INSERT INTO `mto_channels` VALUES ('3', 'questions', '问答', '0');
INSERT INTO `mto_channels` VALUES ('6', 'recommend', '推荐', '0');
INSERT INTO `mto_channels` VALUES ('7', 'find', '发现', '0');

-- ----------------------------
-- Table structure for mto_comments
-- ----------------------------
DROP TABLE IF EXISTS `mto_comments`;
CREATE TABLE `mto_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `author_id` bigint(20) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `pid` bigint(20) NOT NULL,
  `status` int(11) NOT NULL,
  `to_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mto_comments
-- ----------------------------
INSERT INTO `mto_comments` VALUES ('1', '1', '的撒', '2018-04-22 12:22:07', '0', '0', '1');
INSERT INTO `mto_comments` VALUES ('2', '1', '[疑问]', '2018-04-22 12:22:42', '0', '0', '1');
INSERT INTO `mto_comments` VALUES ('3', '1', '你好打算干什么呀，今天天气真的好呀[睡]', '2018-04-27 15:43:15', '0', '0', '3');

-- ----------------------------
-- Table structure for mto_config
-- ----------------------------
DROP TABLE IF EXISTS `mto_config`;
CREATE TABLE `mto_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key_` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_99vo6d7ci4wlxruo3gd0q2jq8` (`key_`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_config
-- ----------------------------
INSERT INTO `mto_config` VALUES ('1', 'site_name', '0', 'Mtons');
INSERT INTO `mto_config` VALUES ('3', 'site_domain', '0', 'http://mtons.com');
INSERT INTO `mto_config` VALUES ('4', 'site_keywords', '0', 'mtons,博客,社区,摄影,旅游,艺术,娱乐');
INSERT INTO `mto_config` VALUES ('5', 'site_description', '0', 'Mtons, 轻松分享你的兴趣. 便捷的文字、图片发布,扁平化的响应式设计,美观、大气,是您记录生活的最佳选择');
INSERT INTO `mto_config` VALUES ('6', 'site_editor', '1', 'ueditor');
INSERT INTO `mto_config` VALUES ('7', 'site_metas', '0', '');
INSERT INTO `mto_config` VALUES ('8', 'site_copyright', '0', 'Copyright © Mtons');
INSERT INTO `mto_config` VALUES ('9', 'site_icp', '0', '');
INSERT INTO `mto_config` VALUES ('10', 'site_advs_right', '0', '');
INSERT INTO `mto_config` VALUES ('11', 'image_processor', '0', 'Thumbnailator');
INSERT INTO `mto_config` VALUES ('12', 'site_oauth_qq', '0', '');
INSERT INTO `mto_config` VALUES ('13', 'qq_app_id', '0', '');
INSERT INTO `mto_config` VALUES ('14', 'qq_app_key', '0', '');
INSERT INTO `mto_config` VALUES ('15', 'site_oauth_weibo', '0', '');
INSERT INTO `mto_config` VALUES ('16', 'weibo_client_id', '0', '');
INSERT INTO `mto_config` VALUES ('17', 'weibo_client_sercret', '0', '');
INSERT INTO `mto_config` VALUES ('18', 'site_oauth_douban', '0', '');
INSERT INTO `mto_config` VALUES ('19', 'douban_api_key', '0', '');
INSERT INTO `mto_config` VALUES ('20', 'douban_secret_key', '0', '');

-- ----------------------------
-- Table structure for mto_element
-- ----------------------------
DROP TABLE IF EXISTS `mto_element`;
CREATE TABLE `mto_element` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parent` bigint(20) NOT NULL,
  `intro` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_element
-- ----------------------------
INSERT INTO `mto_element` VALUES ('1', '游戏类型', '0', null, null);
INSERT INTO `mto_element` VALUES ('2', '游戏平台', '0', null, null);
INSERT INTO `mto_element` VALUES ('3', '游戏厂商', '0', null, null);
INSERT INTO `mto_element` VALUES ('4', '游戏题材', '0', null, null);
INSERT INTO `mto_element` VALUES ('5', '游戏开发', '0', null, null);
INSERT INTO `mto_element` VALUES ('6', 'Windows', '2', null, null);
INSERT INTO `mto_element` VALUES ('7', 'iOS', '2', null, null);
INSERT INTO `mto_element` VALUES ('8', 'Android', '2', null, null);
INSERT INTO `mto_element` VALUES ('9', 'PS', '2', null, null);
INSERT INTO `mto_element` VALUES ('10', 'PS2', '2', null, null);
INSERT INTO `mto_element` VALUES ('11', 'PS3', '2', null, null);
INSERT INTO `mto_element` VALUES ('12', 'PS4', '2', null, null);
INSERT INTO `mto_element` VALUES ('13', 'PSP', '2', null, null);
INSERT INTO `mto_element` VALUES ('14', 'PSV', '2', null, null);
INSERT INTO `mto_element` VALUES ('15', 'Xbox', '2', null, null);
INSERT INTO `mto_element` VALUES ('16', 'Xbox 360', '2', null, null);
INSERT INTO `mto_element` VALUES ('17', 'Xbox One', '2', null, null);
INSERT INTO `mto_element` VALUES ('18', 'FC', '2', null, null);
INSERT INTO `mto_element` VALUES ('19', 'SFC', '2', null, null);
INSERT INTO `mto_element` VALUES ('20', 'N64', '2', null, null);
INSERT INTO `mto_element` VALUES ('21', 'NGPC', '2', null, null);
INSERT INTO `mto_element` VALUES ('22', 'Wii', '2', null, null);
INSERT INTO `mto_element` VALUES ('23', 'Wii U', '2', null, null);
INSERT INTO `mto_element` VALUES ('24', 'Nintendo Switch', '2', null, null);
INSERT INTO `mto_element` VALUES ('25', 'Virtual Boy', '2', null, null);
INSERT INTO `mto_element` VALUES ('26', 'GB', '2', null, null);
INSERT INTO `mto_element` VALUES ('27', 'GBC', '2', null, null);
INSERT INTO `mto_element` VALUES ('28', 'GBA', '2', null, null);
INSERT INTO `mto_element` VALUES ('29', 'NDS', '2', null, null);
INSERT INTO `mto_element` VALUES ('30', '3DS', '2', null, null);
INSERT INTO `mto_element` VALUES ('31', 'SMS', '2', null, null);
INSERT INTO `mto_element` VALUES ('32', 'MD', '2', null, null);
INSERT INTO `mto_element` VALUES ('33', 'Sega CD', '2', null, null);
INSERT INTO `mto_element` VALUES ('34', 'SS', '2', null, null);
INSERT INTO `mto_element` VALUES ('35', 'DC', '2', null, null);
INSERT INTO `mto_element` VALUES ('36', 'Game Gear', '2', null, null);
INSERT INTO `mto_element` VALUES ('37', 'WSC', '2', null, null);
INSERT INTO `mto_element` VALUES ('38', 'NGPC', '2', null, null);
INSERT INTO `mto_element` VALUES ('39', 'PC Engine', '2', null, null);
INSERT INTO `mto_element` VALUES ('40', 'PC-FX', '2', null, null);
INSERT INTO `mto_element` VALUES ('41', 'Neo Geo CD', '2', null, null);
INSERT INTO `mto_element` VALUES ('42', '街机', '2', null, null);
INSERT INTO `mto_element` VALUES ('43', '桌面游戏	', '2', null, null);
INSERT INTO `mto_element` VALUES ('44', 'Mac', '2', null, null);
INSERT INTO `mto_element` VALUES ('45', 'Linux', '2', null, null);
INSERT INTO `mto_element` VALUES ('46', 'DOS', '2', null, null);

-- ----------------------------
-- Table structure for mto_evaluation
-- ----------------------------
DROP TABLE IF EXISTS `mto_evaluation`;
CREATE TABLE `mto_evaluation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `author_id` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `socre` int(11) NOT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `game_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKsu3os1ecwm4ehedu0ivog112w` (`game_id`),
  CONSTRAINT `FKsu3os1ecwm4ehedu0ivog112w` FOREIGN KEY (`game_id`) REFERENCES `mto_game` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_evaluation
-- ----------------------------
INSERT INTO `mto_evaluation` VALUES ('1', '1', '2018-05-02 13:40:42', 'ueditor', '10', null, '游戏测', '5');

-- ----------------------------
-- Table structure for mto_favors
-- ----------------------------
DROP TABLE IF EXISTS `mto_favors`;
CREATE TABLE `mto_favors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `own_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mto_favors
-- ----------------------------
INSERT INTO `mto_favors` VALUES ('1', '2018-04-22 12:23:25', '1', '1');
INSERT INTO `mto_favors` VALUES ('2', '2018-04-22 13:09:39', '1', '2');
INSERT INTO `mto_favors` VALUES ('3', '2018-04-27 15:42:03', '1', '3');
INSERT INTO `mto_favors` VALUES ('4', '2018-05-06 15:02:36', '1', '6');

-- ----------------------------
-- Table structure for mto_feeds
-- ----------------------------
DROP TABLE IF EXISTS `mto_feeds`;
CREATE TABLE `mto_feeds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `author_id` bigint(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `own_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mto_feeds
-- ----------------------------
INSERT INTO `mto_feeds` VALUES ('1', '1', '2018-04-22 12:21:15', '1', '1', '1');
INSERT INTO `mto_feeds` VALUES ('2', '1', '2018-04-22 13:08:43', '1', '2', '1');
INSERT INTO `mto_feeds` VALUES ('3', '1', '2018-04-27 15:41:57', '1', '3', '1');
INSERT INTO `mto_feeds` VALUES ('4', '1', '2018-05-06 14:38:00', '1', '4', '1');
INSERT INTO `mto_feeds` VALUES ('5', '1', '2018-05-06 15:00:10', '1', '5', '1');
INSERT INTO `mto_feeds` VALUES ('6', '1', '2018-05-06 15:00:52', '1', '6', '1');

-- ----------------------------
-- Table structure for mto_follows
-- ----------------------------
DROP TABLE IF EXISTS `mto_follows`;
CREATE TABLE `mto_follows` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `follow_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKlbcc3hcj1cikyow8cvlk1eupe` (`follow_id`),
  KEY `FKso66aluvvri4r5a5x3lh31t8s` (`user_id`),
  CONSTRAINT `FKlbcc3hcj1cikyow8cvlk1eupe` FOREIGN KEY (`follow_id`) REFERENCES `mto_users` (`id`),
  CONSTRAINT `FKso66aluvvri4r5a5x3lh31t8s` FOREIGN KEY (`user_id`) REFERENCES `mto_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mto_follows
-- ----------------------------

-- ----------------------------
-- Table structure for mto_follow_element
-- ----------------------------
DROP TABLE IF EXISTS `mto_follow_element`;
CREATE TABLE `mto_follow_element` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `follow_element_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKl3xjnkcorssls9vcdpfuejn3o` (`follow_element_id`),
  KEY `FKfas3eisu2xqufqt5mu2djlcph` (`user_id`),
  CONSTRAINT `FKfas3eisu2xqufqt5mu2djlcph` FOREIGN KEY (`user_id`) REFERENCES `mto_users` (`id`),
  CONSTRAINT `FKl3xjnkcorssls9vcdpfuejn3o` FOREIGN KEY (`follow_element_id`) REFERENCES `mto_element` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_follow_element
-- ----------------------------

-- ----------------------------
-- Table structure for mto_follow_game
-- ----------------------------
DROP TABLE IF EXISTS `mto_follow_game`;
CREATE TABLE `mto_follow_game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `follow_game_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqxnn0b77vom4q2agao5xtcwya` (`follow_game_id`),
  KEY `FK7hy1ts1ywibun890mh8ny1dpv` (`user_id`),
  CONSTRAINT `FK7hy1ts1ywibun890mh8ny1dpv` FOREIGN KEY (`user_id`) REFERENCES `mto_users` (`id`),
  CONSTRAINT `FKqxnn0b77vom4q2agao5xtcwya` FOREIGN KEY (`follow_game_id`) REFERENCES `mto_game` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_follow_game
-- ----------------------------

-- ----------------------------
-- Table structure for mto_game
-- ----------------------------
DROP TABLE IF EXISTS `mto_game`;
CREATE TABLE `mto_game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `day` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `introduction` varchar(255) DEFAULT NULL,
  `month` int(11) NOT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `score` double NOT NULL,
  `year` int(11) NOT NULL,
  `created` date DEFAULT NULL,
  `creator` bigint(20) NOT NULL,
  `pictures` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_kiid5ljpdaf23rrw9mkhgq3g6` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_game
-- ----------------------------
INSERT INTO `mto_game` VALUES ('5', '3', '/store/thumbs/2018/0428/281326189c58.jpg', '哈哈哈', '2', null, '0', '2020', null, '1', '/store/pictures/5', '测试游戏123');
INSERT INTO `mto_game` VALUES ('6', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/6', '测试游戏6');
INSERT INTO `mto_game` VALUES ('7', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/7', '测试游戏7');
INSERT INTO `mto_game` VALUES ('8', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/8', '测试游戏8');
INSERT INTO `mto_game` VALUES ('9', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/9', '测试游戏9');
INSERT INTO `mto_game` VALUES ('10', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/10', '测试游戏10');
INSERT INTO `mto_game` VALUES ('11', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/11', '测试游戏11');
INSERT INTO `mto_game` VALUES ('12', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/12', '测试游戏12');
INSERT INTO `mto_game` VALUES ('13', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/13', '测试游戏13');
INSERT INTO `mto_game` VALUES ('14', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/14', '测试游戏14');
INSERT INTO `mto_game` VALUES ('15', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/15', '测试游戏15');
INSERT INTO `mto_game` VALUES ('16', '3', '/store/thumbs/2018/0428/281326189c58.jpg', null, '2', null, '0', '2020', null, '1', '/store/pictures/16', '测试游戏16');
INSERT INTO `mto_game` VALUES ('17', '3', '/store/thumbs/2018/0428/281326189c58.jpg', '哈哈哈', '2', null, '0', '2020', null, '1', '/store/pictures/17', '测试游戏17');

-- ----------------------------
-- Table structure for mto_game_element
-- ----------------------------
DROP TABLE IF EXISTS `mto_game_element`;
CREATE TABLE `mto_game_element` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `element_id` bigint(20) NOT NULL,
  `game_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKaamhvfe1no1qydm1w3k2sdaeq` (`element_id`),
  KEY `FKi2qjlalytyaqcxyr4am6mbyu1` (`game_id`),
  CONSTRAINT `FKaamhvfe1no1qydm1w3k2sdaeq` FOREIGN KEY (`element_id`) REFERENCES `mto_element` (`id`),
  CONSTRAINT `FKi2qjlalytyaqcxyr4am6mbyu1` FOREIGN KEY (`game_id`) REFERENCES `mto_game` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_game_element
-- ----------------------------
INSERT INTO `mto_game_element` VALUES ('1', '2018-04-28 13:26:30', '6', '5');
INSERT INTO `mto_game_element` VALUES ('2', '2018-05-04 08:49:51', '42', '10');
INSERT INTO `mto_game_element` VALUES ('3', '2018-05-04 10:29:31', '6', '17');
INSERT INTO `mto_game_element` VALUES ('4', '2018-05-04 10:29:48', '27', '11');

-- ----------------------------
-- Table structure for mto_logs
-- ----------------------------
DROP TABLE IF EXISTS `mto_logs`;
CREATE TABLE `mto_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` date DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `target_id` bigint(20) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mto_logs
-- ----------------------------

-- ----------------------------
-- Table structure for mto_notify
-- ----------------------------
DROP TABLE IF EXISTS `mto_notify`;
CREATE TABLE `mto_notify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `event` int(11) NOT NULL,
  `from_id` bigint(20) DEFAULT NULL,
  `own_id` bigint(20) DEFAULT NULL,
  `post_id` bigint(20) DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mto_notify
-- ----------------------------
INSERT INTO `mto_notify` VALUES ('1', '2018-04-22 12:23:25', '1', '1', '1', '1', '1');
INSERT INTO `mto_notify` VALUES ('2', '2018-04-22 13:09:39', '1', '1', '1', '2', '1');
INSERT INTO `mto_notify` VALUES ('3', '2018-04-27 15:42:03', '1', '1', '1', '3', '1');
INSERT INTO `mto_notify` VALUES ('4', '2018-04-27 15:43:15', '3', '1', '1', '3', '1');
INSERT INTO `mto_notify` VALUES ('5', '2018-05-06 15:02:36', '1', '1', '1', '6', '1');

-- ----------------------------
-- Table structure for mto_posts
-- ----------------------------
DROP TABLE IF EXISTS `mto_posts`;
CREATE TABLE `mto_posts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `author_id` bigint(20) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `comments` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `favors` int(11) NOT NULL,
  `featured` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `summary` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  `views` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `game_id` bigint(20) DEFAULT NULL,
  `socre` int(11) NOT NULL,
  `element_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mto_posts
-- ----------------------------
INSERT INTO `mto_posts` VALUES ('2', '1', '1', '0', '2018-04-22 13:08:42', 'ueditor', '1', '0', '8', '', 'das', 'dsa ', '16', '0', '17', '10', '0');
INSERT INTO `mto_posts` VALUES ('3', '1', '1', '1', '2018-05-02 15:41:56', 'ueditor', '16', '0', '8', '打算，Spring in action啊，一个共享的平台', '', '大', '10', '0', '17', '10', '0');
INSERT INTO `mto_posts` VALUES ('4', '1', '6', '0', '2018-05-06 14:37:59', 'ueditor', '0', '0', '8', '版本的凯撒', '的撒的,的撒', '你好啊', '4', '0', '17', '1', '0');
INSERT INTO `mto_posts` VALUES ('5', '1', '7', '0', '2018-05-06 15:00:10', 'ueditor', '0', '0', '0', 'dsadsa sadas', '', 'ddsadasdas ', '0', '0', '0', '0', '0');
INSERT INTO `mto_posts` VALUES ('6', '1', '7', '0', '2018-05-06 15:00:52', 'ueditor', '5', '0', '0', '电话费撒谎的时间了咯大厦看了就会觉得是打撒打撒年扣篮大赛你考虑年弗兰克三分卢卡斯打扫房 间 昆士兰法律上看付款了三份看来是发你撒娇立刻返回NSA立刻解放拉萨模范中学的哈萨克浪费努力克服 拿撒勒 孔繁森克 里夫封杀 克里夫哈 克上了飞 机离 开 洒...', '', 'd啊大大', '4', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for mto_posts_attribute
-- ----------------------------
DROP TABLE IF EXISTS `mto_posts_attribute`;
CREATE TABLE `mto_posts_attribute` (
  `id` bigint(20) NOT NULL,
  `content` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mto_posts_attribute
-- ----------------------------
INSERT INTO `mto_posts_attribute` VALUES ('1', '<p>打算dsadddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd</p>');
INSERT INTO `mto_posts_attribute` VALUES ('2', '<p><img src=\"../store/thumbs/2018/0422/221308390st2.png\" /><img src=\"../store/thumbs/2018/0422/221308390st2.png\" /></p>');
INSERT INTO `mto_posts_attribute` VALUES ('3', '<p>打算，Spring in action啊，一个共享的平台,发动机阿卡发货师傅你好撒九分裤雷锋精神多了快捷方式大力福利卡是否建立的撒会计分录发飞机撒地方积分离开静安寺了房间里的的点点滴滴多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多多的点点滴滴多多多多多多多多多多多多多多多多多多多多多的点点滴滴多多多多多多多多多多多多多多</p>');
INSERT INTO `mto_posts_attribute` VALUES ('4', '<p>版本的凯撒<img src=\"../store/thumbs/2018/0422/221308390st2.png\" /></p>');
INSERT INTO `mto_posts_attribute` VALUES ('5', '<p>dsadsa sadas<img src=\"../store/thumbs/2018/0506/06150007cwmx.png\" /></p>');
INSERT INTO `mto_posts_attribute` VALUES ('6', '<p>电话费撒谎的时间了咯大厦看了就会觉得是打撒打撒年扣篮大赛你考虑年弗兰克三分卢卡斯打扫房</p>\r\n<p>间</p>\r\n<p>昆士兰法律上看付款了三份看来是发你撒娇立刻返回NSA立刻解放拉萨模范中学的哈萨克浪费努力克服</p>\r\n<p>拿撒勒</p>\r\n<p>孔繁森克</p>\r\n<p>里夫封杀</p>\r\n<p>克里夫哈</p>\r\n<p>克上了飞</p>\r\n<p>机离</p>\r\n<p>开</p>\r\n<p>洒家</p>\r\n<p>疯</p>\r\n<p>狂</p>\r\n<p>&nbsp;</p>\r\n<p>拉</p>\r\n<p>升飞</p>\r\n<p>洒</p>\r\n<p>&nbsp;</p>\r\n<p>飞洒</p>');

-- ----------------------------
-- Table structure for mto_post_game
-- ----------------------------
DROP TABLE IF EXISTS `mto_post_game`;
CREATE TABLE `mto_post_game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `element_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK99noeolftob80nrjcihofjr29` (`element_id`),
  KEY `FKjkrudoxj0pvrimgnvj21xeiw7` (`post_id`),
  CONSTRAINT `FK99noeolftob80nrjcihofjr29` FOREIGN KEY (`element_id`) REFERENCES `mto_element` (`id`),
  CONSTRAINT `FKjkrudoxj0pvrimgnvj21xeiw7` FOREIGN KEY (`post_id`) REFERENCES `mto_posts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_post_game
-- ----------------------------

-- ----------------------------
-- Table structure for mto_role
-- ----------------------------
DROP TABLE IF EXISTS `mto_role`;
CREATE TABLE `mto_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_role
-- ----------------------------
INSERT INTO `mto_role` VALUES ('1', '管理员');
INSERT INTO `mto_role` VALUES ('2', '普通用户');
INSERT INTO `mto_role` VALUES ('3', '半个管理员');

-- ----------------------------
-- Table structure for mto_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `mto_role_menu`;
CREATE TABLE `mto_role_menu` (
  `role_id` bigint(20) NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  KEY `FK_5o5vaxfyg0d1qa0142dnkruiv` (`role_id`),
  KEY `FK1enh5yen34dpmson36gn7peq2` (`menu_id`),
  CONSTRAINT `FK1enh5yen34dpmson36gn7peq2` FOREIGN KEY (`menu_id`) REFERENCES `mto_auth_menu` (`id`),
  CONSTRAINT `FK4c4m4bpj3t1f4ovq7y36n5dq7` FOREIGN KEY (`role_id`) REFERENCES `mto_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_role_menu
-- ----------------------------
INSERT INTO `mto_role_menu` VALUES ('1', '1');
INSERT INTO `mto_role_menu` VALUES ('1', '2');
INSERT INTO `mto_role_menu` VALUES ('1', '4');
INSERT INTO `mto_role_menu` VALUES ('1', '5');
INSERT INTO `mto_role_menu` VALUES ('1', '6');
INSERT INTO `mto_role_menu` VALUES ('1', '8');
INSERT INTO `mto_role_menu` VALUES ('1', '9');
INSERT INTO `mto_role_menu` VALUES ('1', '10');
INSERT INTO `mto_role_menu` VALUES ('1', '11');
INSERT INTO `mto_role_menu` VALUES ('1', '12');
INSERT INTO `mto_role_menu` VALUES ('1', '13');
INSERT INTO `mto_role_menu` VALUES ('1', '15');
INSERT INTO `mto_role_menu` VALUES ('1', '16');
INSERT INTO `mto_role_menu` VALUES ('1', '17');
INSERT INTO `mto_role_menu` VALUES ('1', '18');
INSERT INTO `mto_role_menu` VALUES ('1', '19');
INSERT INTO `mto_role_menu` VALUES ('1', '20');
INSERT INTO `mto_role_menu` VALUES ('1', '35');
INSERT INTO `mto_role_menu` VALUES ('1', '36');
INSERT INTO `mto_role_menu` VALUES ('1', '37');
INSERT INTO `mto_role_menu` VALUES ('1', '41');
INSERT INTO `mto_role_menu` VALUES ('1', '42');
INSERT INTO `mto_role_menu` VALUES ('1', '43');

-- ----------------------------
-- Table structure for mto_users
-- ----------------------------
DROP TABLE IF EXISTS `mto_users`;
CREATE TABLE `mto_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `username` varchar(64) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT '/assets/images/ava/default.png',
  `updated` datetime DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `source` int(11) NOT NULL,
  `active_email` int(11) DEFAULT NULL,
  `comments` int(11) NOT NULL,
  `fans` int(11) NOT NULL,
  `favors` int(11) NOT NULL,
  `follows` int(11) NOT NULL,
  `posts` int(11) NOT NULL,
  `signature` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_users
-- ----------------------------
INSERT INTO `mto_users` VALUES ('1', '2017-08-06 17:52:41', 'example@mtons.com', '2018-05-09 08:49:31', null, '3TGCQF25BLHU9R7IQUITN0FCC5', '0', 'admin', '小豆丁', '/store/ava/000/00/00/01_100.jpg', '2017-07-26 11:08:36', '0', '1', '0', '0', '9', '-2', '0', '-2', '25', '');
INSERT INTO `mto_users` VALUES ('2', '2018-04-21 13:47:18', 'fantasy9417@qq.com', '2018-04-22 12:31:16', null, 'UUKHSDDI5KPA43A8VL06V0TU2', '0', 'syj9417', '哈哈', '/dist/images/ava/default.png', null, '0', null, '0', '0', '0', '0', '0', '0', '0', null);

-- ----------------------------
-- Table structure for mto_users_open_oauth
-- ----------------------------
DROP TABLE IF EXISTS `mto_users_open_oauth`;
CREATE TABLE `mto_users_open_oauth` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(255) DEFAULT NULL,
  `expire_in` varchar(255) DEFAULT NULL,
  `oauth_code` varchar(255) DEFAULT NULL,
  `oauth_type` int(11) DEFAULT NULL,
  `oauth_user_id` varchar(255) DEFAULT NULL,
  `refresh_token` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_users_open_oauth
-- ----------------------------

-- ----------------------------
-- Table structure for mto_user_role
-- ----------------------------
DROP TABLE IF EXISTS `mto_user_role`;
CREATE TABLE `mto_user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  KEY `FK_fhtla2vc199mv0ru2r2kvakha` (`role_id`),
  KEY `FK_b4m7ef0uvkr4efrscf8r1ehy2` (`user_id`),
  CONSTRAINT `FK_b4m7ef0uvkr4efrscf8r1ehy2` FOREIGN KEY (`user_id`) REFERENCES `mto_users` (`id`),
  CONSTRAINT `FK_fhtla2vc199mv0ru2r2kvakha` FOREIGN KEY (`role_id`) REFERENCES `mto_role` (`id`),
  CONSTRAINT `FKi5bot5saeg7e7fkjleer6cl0q` FOREIGN KEY (`user_id`) REFERENCES `mto_users` (`id`),
  CONSTRAINT `FKklvl5j02s6eorfu88bbvr2e7x` FOREIGN KEY (`role_id`) REFERENCES `mto_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mto_user_role
-- ----------------------------
INSERT INTO `mto_user_role` VALUES ('1', '1');

-- ----------------------------
-- Table structure for mto_verify
-- ----------------------------
DROP TABLE IF EXISTS `mto_verify`;
CREATE TABLE `mto_verify` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `expired` datetime NOT NULL,
  `status` int(11) DEFAULT NULL,
  `target` varchar(96) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_m7p0b526c4xlgjn787t22om2g` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mto_verify
-- ----------------------------
INSERT INTO `mto_verify` VALUES ('1', '0384819975', '2018-04-21 13:47:19', '2018-04-21 14:17:19', '0', 'fantasy9417@qq.com', null, '1', '2');
