package mblog.core.persist.service;

import mblog.core.data.Game;
import mblog.core.persist.entity.GamePo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface GameService {
    long save(Game game);
    Game show(long gameId);
    /**
     * 分页查询游戏
     * */
    List<Game> paging(Pageable pageable);
    long count();

    /**
     * 所有游戏
     * */
    List<GamePo> games();

    /**
     * 按名字搜索游戏
     * */
    List<GamePo> games(String word);

}
