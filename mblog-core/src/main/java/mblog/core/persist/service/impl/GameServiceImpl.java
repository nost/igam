package mblog.core.persist.service.impl;

import mblog.core.data.Game;
import mblog.core.persist.dao.*;
import mblog.core.persist.entity.ElementPo;
import mblog.core.persist.entity.GameElementPo;
import mblog.core.persist.entity.GamePo;
import mblog.core.persist.service.GameElementService;
import mblog.core.persist.service.GameService;
import mblog.core.persist.utils.FileUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author syj
 */
@Service
@Transactional
public class GameServiceImpl implements GameService{
    @Autowired
    private GameDao gameDao;

    @Autowired
    private ElementDao elementDao;
    @Autowired
    private GameElementDao gameElementDao;
    @Autowired
    private GameElementService gameElementService;
    @Autowired
    private PostDao postDao;



    @Override
    public long save(Game game) {
        GamePo gamePo = new GamePo();
        BeanUtils.copyProperties(game,gamePo);
        gamePo.setCreated(new Date());
        long gameId = gameDao.save(gamePo).getId();
        File file = new File("/store/pictures/"+gameId);
        FileUtil.makeDirectory(file);
        gamePo.setPictures("/store/pictures/"+gameId);
        gameDao.save(gamePo);
        gameElementService.saveGameElement(gamePo,game.getPlatform());
        gameElementService.saveGameElement(gamePo,game.getElement());
        return gameId;
    }

    @Override
    public Game show(long gameId) {
        GamePo gamePo = gameDao.findOne(gameId);
        return cast(gamePo);

    }

    @Override
    public List<Game> paging(Pageable pageable) {
        Page<GamePo> gamePos = gameDao.findAll(pageable);
        List<Game> games = new ArrayList<>();
        Game game = new Game();
        for (GamePo gamePo : gamePos){
            game = cast(gamePo);
            games.add(game);
        }
        return games;
    }

    @Override
    public long count() {
        return gameDao.count();
    }

    @Override
    public List<GamePo> games() {
        return gameDao.findAll();
    }

    @Override
    public List<GamePo> games(String word) {
        return gameDao.findByNameContaining(word);
    }

    public Game cast(GamePo gamePo){
        Game game = new Game();
        BeanUtils.copyProperties(gamePo,game);
        List<GameElementPo> gameElementPos = gameElementDao.findAllByGamePo(gamePo);
        List<ElementPo> platforms = new ArrayList<>();
        List<ElementPo> elements = new ArrayList<>();
        /**
         * 给game添加平台和元素
         * */
        for (GameElementPo gameElementPo : gameElementPos){
            if (gameElementPo.getElementPo().getParent() == 2){
                platforms.add(gameElementPo.getElementPo());
            }else {
                elements.add(gameElementPo.getElementPo());
            }
        }
        game.setEvaluation(postDao.findByGameIdAndStatus(gamePo.getId(),8).size());
        game.setElements(elements);
        game.setPlatforms(platforms);

        /**
         * 给游戏添加测评 攻略 的数量
         * */
        game.setPost(postDao.findByGameIdAndStatusNotAndStatusNot(gamePo.getId(),9,8).size());
        game.setEvaluation(postDao.findByGameIdAndStatus(gamePo.getId(),9).size());
        String pith = "D:\\code\\mblog\\mblog-web\\src\\main\\webapp";
        File file = new File(pith+gamePo.getPictures());
        List<String> list = new ArrayList<>();
        FileUtil.getAllFile(gamePo.getId(),file,list);
        game.setPicturesUrl(list);
        System.out.print(game.getPicturesUrl().size());
        return game;
    }

}
