package mblog.core.persist.service;

import mblog.core.persist.entity.ElementPo;

import java.util.List;


public interface ElementService {
    List<ElementPo> elementPos();
    List<ElementPo> elementPos(String word);
}
