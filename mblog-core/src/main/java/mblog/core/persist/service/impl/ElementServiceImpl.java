package mblog.core.persist.service.impl;

import mblog.core.persist.dao.ElementDao;
import mblog.core.persist.entity.ElementPo;
import mblog.core.persist.service.ElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ElementServiceImpl implements ElementService{
    @Autowired
    ElementDao elementDao;

    @Override
    public List<ElementPo> elementPos(){
        return elementDao.findAll();
    }

    @Override
    public List<ElementPo> elementPos(String word) {
        return elementDao.findAllByNameContaining(word);
    }
}
