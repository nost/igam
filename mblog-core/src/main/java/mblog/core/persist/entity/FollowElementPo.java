package mblog.core.persist.entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;

/**
 * 对元素的关注
 * */
@Entity
@Table(name = "mto_follow_element")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FollowElementPo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * 所属用户Id
     */
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserPO user;

    /**
     * 关注元素Id
     * */
    @ManyToOne
    @JoinColumn(name = "follow_element_id", nullable = false)
    private ElementPo flowElemet;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserPO getUser() {
        return user;
    }

    public void setUser(UserPO user) {
        this.user = user;
    }

    public ElementPo getFlowElemet() {
        return flowElemet;
    }

    public void setFlowElemet(ElementPo flowElemet) {
        this.flowElemet = flowElemet;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
