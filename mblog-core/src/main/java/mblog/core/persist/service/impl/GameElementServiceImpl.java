package mblog.core.persist.service.impl;

import mblog.core.persist.dao.ElementDao;
import mblog.core.persist.dao.GameElementDao;
import mblog.core.persist.entity.ElementPo;
import mblog.core.persist.entity.GameElementPo;
import mblog.core.persist.entity.GamePo;
import mblog.core.persist.service.GameElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional
public class GameElementServiceImpl implements GameElementService{
    @Autowired
    ElementDao elementDao;

    @Autowired
    GameElementDao gameElementDao;

    @Override
    public void saveGameElement(GamePo gamePo, long[] elementIds) {
        for (long elementId: elementIds) {
            ElementPo elementPo = elementDao.findOne(elementId);
            System.out.println(elementPo.getName());
            GameElementPo gameElementPo = new GameElementPo();
            gameElementPo.setGamePo(gamePo);
            gameElementPo.setElementPo(elementPo);
            gameElementPo.setCreated(new Date());
            gameElementDao.save(gameElementPo);
        }
    }
}
