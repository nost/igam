<#include "/default/utils/ui.ftl"/>
<@layout "写文章">
<!--suppress ALL -->
<link rel="stylesheet" href="../../res/layui/css/layui.css">
<link rel="stylesheet" href="../my/css/tag.css">
<link rel="stylesheet" href="../my/css/syj.css">
<div id="my" class="panel panel-default" style="margin-top: 50px">
	<div class="panel-heading">
		<i class="icon icon-pencil"></i> 写文章
	</div>
	<div class="panel-body layui-form">
		<div id="message"></div>
		<form class="form-horizontal" action="${base}/post/submit" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">标题</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="title" maxlength="128" data-required >
				</div>
			</div>
            <div class="form-group">
                <label class="col-sm-2 control-label no-padding-right">发布到</label>
                <div class="col-sm-3">
                    <select class="form-control" name="channelId">
						<#list channels as row>
                        <option value="${row.id}">${row.name}</option>
						</#list>
                    </select>
                </div>
            </div>
            <div class="form-group" id="element-input">
                <label class="col-sm-2 control-label no-padding-right">相关元素</label>
                <div class="col-sm-3">
                    <input type="text" name="title" required lay-verify="required"autocomplete="off"
						   class="layui-input" v-on:keyup="search_element" v-model="s_element">
                    <ul class="select-menu-ul" v-bind:style="el_diaplay">
                        <li v-for="el in list_element" v-bind:data-elementId="el.id" v-on:click="addEl(el.id,el.name)">{{el.name}}</li>
                    </ul>
                </div>
				<div v-for="el in select_element" style="float: left">
					<input style="display: none" v-bind:value="el.id" name="elementId">
					<p style="float: left;margin-right: 10px">{{el.name}}</p>
					<i class="remove-over fs12 fa fa-remove" v-on:click="delEl"></i>
				</div>
            </div>
            <div class="form-group" id="game-input">
                <label class="col-sm-2 control-label no-padding-right">相关游戏</label>
                <div class="col-sm-3">
                    <input type="text" name="title" required lay-verify="required" autocomplete="off"
                           class="layui-input" v-on:keyup="search_game" v-model="s_game">
                    <ul class="select-menu-ul" v-bind:style="game_diaplay">
                        <li v-for="game in list_game" v-bind:data-gameId="game.id" v-on:click="addGame(game.id,game.name)">{{game.name}}</li>
                    </ul>
                </div>
                <div v-for="game in select_game" style="float: left">
                    <input style="display: none" v-bind:value="game.id" name="gameId">
                    <p style="float: left;margin-right: 10px">{{game.name}}</p>
                    <i class="remove-over fs12 fa fa-remove" v-on:click="delGame"></i>
                </div>
            </div>
			<div class="form-group">
				<label for="desc" class="col-sm-2 control-label no-padding-right">内容</label>
				<input type="hidden" name="editor" value="${site_editor}"/>
				<div class="col-sm-8">
					<#include "/default/channel/editor/ueditor.ftl"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">标签</label>
				<div class="col-sm-8">
					<input type="hidden" name="tags" id="fieldTags">
					<ul id="tags"></ul>
					<p class="help-block" style="font-size: 12px;">添加相关标签，用逗号或空格分隔 (最多4个).</p>
				</div>
			</div>
			<div class="row">
				<div class="form-group">
					<div class="text-center">
						<button type="submit" class="btn btn-primary">提交</button>
					</div>
				</div>
			</div>
		</form>
		<!-- /form-actions -->
	</div>
</div>
<script src="http://www.jq22.com/jquery/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../../res/layui/layui.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script type="text/javascript">

seajs.use('post', function (post) {
	post.init();
});
layui.use('form', function(){
    var form = layui.form;

    //监听提交
    form.on('submit(formDemo)', function(data){
        layer.msg(JSON.stringify(data.field));
        return false;
    });
});

</script>
<script>
    var search = new Vue({
        el:"#element-input",
        data:{
            el_diaplay:{
                display:"none"
			},
            game_diaplay:{
                display:"none"
            },
            s_element:"",
            s_game:"",
			list_element:"",
			list_game:"",
            select_element:[],
            select_game:[]
        },
        methods:{
            search_element:function (event) {
        	var word = this.s_element;
        	if (word.length == 0 ){
        	    this.el_diaplay = {display:"none"};
			}else {
                this.el_diaplay = {display:"block"};
			}
            fetch("/element/search?word="+word).then(response => response.json() ).then(
                json => {
					this.list_element = json;
				}
			)},
            addEl:function (elementId,elementName) {
                if(!this.ifContains({id:elementId,name:elementName},this.select_element)){
                    this.select_element.push({id:elementId,name:elementName});
                }
                this.el_diaplay = {display:"none"};
            },
            delEl:function(elementId,elementName){
                if(this.ifContains({id:elementId,name:elementName},this.select_element)){
                    this.select_element.pop(this.select_element.find(t => t.id === elementId));
                }
                this.select_element.pop(this.select_element.find(t => t.id === elementId));
            },
            ifContains:function(item,items){
                for(var i of items)
                    if(item.id===i.id) return  true;
                return false;
            }
        }
    })
    var game_search = new Vue({
        el:"#game-input",
        data:{
            game_diaplay:{
                display:"none"
            },
            s_game:"",
            list_game:"",
            select_game:[]
        },
        methods:{
            search_game:function (event) {
                var word = this.s_game;
                if (word.length == 0 ){
                    this.game_diaplay = {display:"none"};
                }else {
                    this.game_diaplay = {display:"block"};
                }
                fetch("/game/search?word="+word).then(response => response.json() ).then(
                        json => {
                    this.list_game = json;
            }
            )},
            addGame:function (gameId,gameName) {
                if(!this.ifContains({id:gameId,name:gameName},this.select_game)){
                    this.select_game.push({id:gameId,name:gameName});
                }
                this.game_diaplay = {display:"none"};
            },
            delGame:function(gameId,gameName){
                if(this.ifContains({id:gameId,name:gameName},this.select_game)){
                    this.select_game.pop(this.select_game.find(t => t.id === gameId));
                }
                this.select_game.pop(this.select_game.find(t => t.id === gameId));
            },
            ifContains:function(item,items){
                for(var i of items)
                    if(item.id===i.id) return  true;
                return false;
            }
        }
    })
</script>
</@layout>