package mblog.core.persist.entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mto_post_game")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PostGamePo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    /**
     * 文章Id
     */
    @ManyToOne
    @JoinColumn(name = "post_id", nullable = false)
    private PostPO post;

    /**
     * 元素Id
     */
    @ManyToOne
    @JoinColumn(name = "element_id", nullable = false)
    private GamePo gamePo;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PostPO getPost() {
        return post;
    }

    public void setPost(PostPO post) {
        this.post = post;
    }

    public GamePo getGamePo() {
        return gamePo;
    }

    public void setGamePo(GamePo gamePo) {
        this.gamePo = gamePo;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
