package mblog.core.persist.dao;

import mblog.core.persist.entity.ElementPo;
import mblog.core.persist.entity.PostElementPo;
import mblog.core.persist.entity.PostGamePo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface PostElementDao extends JpaRepository<PostElementPo,Long>,JpaSpecificationExecutor<PostElementPo>{

}
