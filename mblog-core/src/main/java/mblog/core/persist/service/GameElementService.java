package mblog.core.persist.service;

import mblog.core.persist.entity.GamePo;

public interface GameElementService {
    void saveGameElement(GamePo gamePo, long elementIds[]);
}
