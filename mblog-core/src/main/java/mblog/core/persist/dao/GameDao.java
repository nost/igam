package mblog.core.persist.dao;

import mblog.core.persist.entity.GamePo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;


/**
 * 继承PagingAndSortingRepository实现分页
 * */
public interface GameDao extends JpaRepository<GamePo,Long>,JpaSpecificationExecutor<GamePo>{
    List<GamePo> findByName(String name);
    List<GamePo> findByNameContaining(String word);
    List<GamePo> findByScoreGreaterThan(double soucre);
    List<GamePo> findByCreatedBetween(Date old,Date now);
    @Query(value = "SELECT * FROM mto_game LEFT JOIN mto_posts ON mto_game.id = mto_posts.game_id" +
            "WHERE mto_posts.created BETWEEN ?1 AND ?2"  ,nativeQuery = true)
    List<GamePo> findPopularGame(Date old,Date now);

}
