<#include "/default/utils/ui.ftl"/>
<@layout "写文章">
<link media="screen" href="../../../my/statics/grade.css" type="text/css" rel="stylesheet" />

<div class="panel panel-default">
	<div class="panel-heading">
		<i class="icon icon-pencil"></i> 写评价
	</div>
	<div class="panel-body">
		<div id="message"></div>
		<form class="form-horizontal layui-form" action="${base}/game/review/addReview" method="post" enctype="multipart/form-data">
			<input value="${gameId}" name="gameId" style="display: none">
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">标题</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="title" maxlength="128" data-required >
				</div>
			</div>
			<div class="form-group">
                <div id="box" style="margin-left: 150px">
                    <div class="content">
                        <div id="doPoint">
                            <table cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                <tr>
                                    <td><span class="star5" id="item1" v="5"><small>1</small><small>2</small><small>3</small><small>4</small><small>5</small><small>6</small><small>7</small><small>8</small><small>9</small><small>10</small></span></td>
                                    <td><strong id="scoreView"></strong> <em></em></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
					<input id="score" value="7" name="score" style="display:none;">
                </div>
			</div>
			<div class="form-group">
				<label for="desc" class="col-sm-2 control-label no-padding-right">内容</label>
				<input type="hidden" name="editor" value="${site_editor}"/>
				<div class="col-sm-8">
					<#include "/default/channel/editor/ueditor.ftl"/>
				</div>
			</div>
			<div class="row">
				<div class="form-group">
					<div class="text-center">
						<button type="submit" class="btn btn-primary">提交</button>
					</div>
				</div>
			</div>
		</form>
		<!-- /form-actions -->
	</div>
</div>
<script src="../../../my/statics/jquery-latest.pack.js" type="text/javascript"></script>
<script src="../../../my/statics/grade.js" type="text/javascript"></script>
<script src="../../../dist/js/plugins.js" type="text/javascript"></script>
<script type="text/javascript" src="../../res/layui/layui.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script type="text/javascript">
seajs.use('post', function (post) {
	post.init();
});

var url = "/game/review/addReview";
var  frameindex = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
$.post(url,data,function(ev){
},'json')
$(document).on('click','[type=submit]',function(){
    var index = layer.load(1, {
        shade: [0.1,'#fff'] //0.1透明度的白色背景
    });

});

</script>

</@layout>