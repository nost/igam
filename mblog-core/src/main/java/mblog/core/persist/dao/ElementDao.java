package mblog.core.persist.dao;

import mblog.core.persist.entity.ElementPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ElementDao extends JpaRepository<ElementPo,Long>,JpaSpecificationExecutor<ElementDao>{
    List<ElementPo> findAllByNameContaining(String word);
}
