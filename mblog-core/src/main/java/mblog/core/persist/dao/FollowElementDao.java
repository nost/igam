package mblog.core.persist.dao;

import mblog.core.persist.entity.ElementPo;
import mblog.core.persist.entity.FollowElementPo;
import mblog.core.persist.entity.GamePo;
import mblog.core.persist.entity.UserPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface FollowElementDao extends JpaRepository<FollowElementPo,Long>,JpaSpecificationExecutor<FollowElementPo> {
    List<FollowElementPo> findByUser(UserPO userPO);
    List<FollowElementPo> findByUserAndFlowElemet(UserPO userPO,ElementPo elementPo);
    void deleteByUserAndFlowElemet(UserPO userPO,ElementPo elementPo);

}
