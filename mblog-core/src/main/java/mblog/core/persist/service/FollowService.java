/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/
package mblog.core.persist.service;

import mblog.core.data.User;
import mblog.core.persist.entity.ElementPo;
import mblog.core.persist.entity.GamePo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author langhsu
 *
 */
public interface FollowService {
	/**
	 * 关注某用户
	 * @param userId
	 * @param followId
	 * @return
	 */
	long follow(long userId, long followId);

	/**
	 * 取消关注某用户
	 * @param userId
	 * @param followId
	 */
	void unfollow(long userId, long followId);

	/**
	 * 查询我的关注
	 *
	 * @param pageable
	 * @param userId
	 */
	Page<User> follows(Pageable pageable, long userId);

	/**
	 * 查询关注我的 (我的粉丝)
	 *
	 * @param pageable
	 * @param userId
	 */
	Page<User> fans(Pageable pageable, long userId);

	/**
	 * 检查是否已关注
	 *
	 * @param userId
	 * @param followId
	 * @return true:已关注
	 */
	boolean checkFollow(long userId, long followId);

	/**
	 * 检查是否相互关注
	 *
	 * @param userId
	 * @param targetUserId
	 * @return true:相互关注
	 */
	boolean checkCrossFollow(long userId, long targetUserId);

	/**
	 * 我关注的游戏
	 * */
	List<GamePo> myFollowGame(long userId);

	/**
	 * 我关注的条目
	 * */
	List<ElementPo> myFollowElement(long userId);

	/**
	 * 关注游戏
	 * */
	void followGame(long userId,long gameId);

	/**
	 * 取消关注游戏
	 * */
	void unfollowGame(long userId,long gameId);

	 /**
	  * 是否关注游戏
	  * */
	 boolean checkFollowGame(long userId,long gameId);

	 /**
	  * 关注元素
	  * */
	 void followElement(long userId,long elementId);

	 /**
	  * 取消关注元素
	  * */
	 void unfollowElement(long userId,long elementId);

	 /**
	  * 是否关注条目
	  * */
	 boolean checkFollowElement(long userId,long elementId);

}
