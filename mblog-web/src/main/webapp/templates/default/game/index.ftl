<#include "/default/utils/ui.ftl"/>
<@layout "游戏">
<!--suppress ALL -->
<link rel="stylesheet" type="text/css" href="../my/css/layui.css">
<link rel="stylesheet" href="../../res/layui/css/layui.css">
<link rel="stylesheet" href="../../res/css/global.css">
<link rel="stylesheet" type="text/css" href="../../my/css/base.css">
<link rel="stylesheet" type="text/css" href="../../my/css/common.css">
<link rel="stylesheet" type="text/css" href="../../my/css/custom-theme.css">
<link rel="stylesheet" type="text/css" href="../../my/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="../../my/css/gameV2.css">
<link rel="stylesheet" type="text/css" href="../../my/css/navV3.css">
<link rel="stylesheet" type="text/css" href="../../my/css/pagespeed.co.wREF_Lb4uo.css">
<link rel="stylesheet" type="text/css" href="../../my/css/swiper-3.4.2.min.css">
<link rel="stylesheet" type="text/css" href="../../my/css/syj.css">
<div class="fly-panel" pad20 style="padding-top: 5px;">
    <div id="gameA">
        <section>
            <div class="flex-box fs20 lhi flex-align-items-center">
                <div class="flex-box flex-item-1">
                    <a class="tab-item mr20 syj-on syj-bigfont">最近流行</a>
                    <a class="tab-item mr20 syj-bigfont">新品</a>
                    <a class="tab-item mr20 syj-bigfont">高分</a>
                    <a class="tab-item mr20 syj-bigfont">最新创建</a>
                    <a class="tab-item mr20 syj-bigfont">综合排序</a></div>
                <a class="layui-btn layui-btn-sm " href="/game/add">创建游戏</a>
            </div>
            <div class="flex-box flex-align-items-baseline mt20">
                <span class="tab-item mr20 min_w on syj-on">全部类型</span>
                <div class="flex-box flex-wrap">
                    <span class="tab-item mr20">RPG</span>
                    <span class="tab-item mr20">动作</span>
                    <span class="tab-item mr20">策略</span>
                    <span class="tab-item mr20">模拟</span>
                    <span class="tab-item mr20">模拟经营</span>
                    <span class="tab-item mr20">冒险</span>
                    <span class="tab-item mr20">FPS</span>
                    <span class="tab-item mr20">Roguelike</span>
                    <span class="tab-item mr20">沙盒</span>
                    <span class="tab-item mr20">解谜</span>
                    <span class="tab-item mr20">Galgame</span>
                    <span class="tab-item mr20">JRPG</span>
                    <span class="tab-item mr20" style="display: none;">国产游戏</span>
                    <span class="tab-item mr20" style="display: none;">生存</span>
                    <span class="tab-item mr20" style="display: none;">音乐游戏</span>
                    <span class="tab-item mr20" style="display: none;">平台动作</span>
                    <span class="tab-item mr20" style="display: none;">战棋</span>
                    <span class="tab-item mr20" style="display: none;">射击</span>
                    <span class="tab-item mr20" style="display: none;">即时战略</span>
                    <span class="tab-item mr20" style="display: none;">ARPG</span>
                    <span class="tab-item mr20" style="display: none;">开放世界</span>
                    <span class="tab-item mr20" style="display: none;">像素</span>
                    <span class="tab-item mr20" style="display: none;">格斗</span>
                    <span class="tab-item mr20" style="display: none;">竞速</span>
                    <span class="tab-item mr20" style="display: none;">STG</span>
                    <span class="tab-item mr20" style="display: none;">体育</span>
                    <span class="tab-item mr20" style="display: none;">探索</span>
                    <span class="tab-item mr20" style="display: none;">步行模拟</span>
                    <span class=" tab-item mr20 white3 gl-hover-text-white">+ 更多</span></div>
            </div>
            <div class="flex-box flex-align-items-baseline mt20">
                <span class="tab-item mr20 min_w on syj-on">全部平台</span>
                <div class="flex-box flex-wrap">
                    <span class="tab-item mr20">Windows</span>
                    <span class="tab-item mr20">PS4</span>
                    <span class="tab-item mr20">Nintendo Switch</span>
                    <span class="tab-item mr20">Xbox One</span>
                    <span class="tab-item mr20">iOS</span>
                    <span class="tab-item mr20">Android</span>
                    <span class="tab-item mr20">PSV</span>
                    <span class="tab-item mr20">3DS</span>
                    <span class="tab-item mr20">Mac</span>
                    <span class="tab-item mr20 white3 gl-hover-text-white">+ 更多</span></div>
            </div>
        </section>
        <div class="mt20">
            <div class="vm-game-list-preview flex-box flex-align-items-start">
                <div class="flex-item-1">
                    <#list games as game>
                        <div class="vm-game-list-item flex-box p10 on" data-gameId=${game.id} v-on:mouseenter="inGame($event)">
                            <a class="game-cover block cl-bg2img-outer" target="_blank"
                               data-gameId=${game.id} href="/game/${game.id}">
                                <img class="vm-img-lazy cl-bg2img-inner cl-vm-lazyload-img" lazy="loaded" src=${game.image}>
                            </a>
                            <div class="ml10 flex-box flex-direction-column">
                                <p><a target="_blank" class="gl-hover-text block white fs14 lhi text-overflow-2 fwb"
                                      href="/game/${game.id}">${game.name}</a></p>
                                <div class="white5 flex-box flex-align-items-center">
                                    <span class="mr10">发行于 ${game.year}年${game.month}月${game.day}日</span>
                                    <span class="star-color mr5">${game.score}</span>
                                    <span class="white3 mr10">(${game.evaluation})</span>
                                    <div class="lh1_4 flex-box flex-align-items-center" data-role="tips" tag="928"
                                         allow_show_price_id="928">
                                    </div>
                                </div>
                                <#list game.platforms as pla>
                                    <div class="flex-box flex-wrap">
                                        <a class="white3 platform_support gl-hover-text" target="_blank"
                                           href="/element/${pla.id}">${pla.name}</a>
                                    </div>
                                </#list>
                            </div>
                        </div>
                    </#list>
                </div>
                <div class="vm-game-preview p20">
                    <div class="flex-box">
                        <div class="flex-item-1">
                            <a class="white fs20 fwb" target="_blank" href="/game/God_of_War_IV">{{name}}</a>
                            <dl class="m0 mt10 white flex-box">
                                <dt class="flex-shrink-0">元素：</dt>
                                <dd class="ml0" v-for="element in elements">
                                    <a target="_blank" class="white5 gl-hover-text" data-role="tip-tag" data-x="0"
                                       data-y="27" href="/element/35" data-id="35" >{{element}}</a>
                                    <span class="white5" style="margin: 0 5px">/</span>
                                </dd>
                                </dl>
                            <dl class="m0 mt10 white flex-box">
                                <dt class="flex-shrink-0">平台：</dt>
                                <dd class="ml0">
                                    <a target="_blank" class="white5 gl-hover-text" data-role="tip-tag" data-x="0"
                                       data-y="27" href="/element/6" data-id="6" v-for="pla in plas">
                                        {{pla.name}}
                                    </a>
                                </dd>
                            </dl>
                        </div>
                        <div class="played-score-box ml10">
                            <div class="white3">评分</div>
                            <div class="fs32 star-color fontFace-Avenir lhi">
                                    {{score}}
                                <span class="mr5 ml5 white3 fs12">/</span>
                                <span class="white3 fs12">10</span></div>
                            <div class="blue nohover fs12 lhi" style="visibility: inherit;">
                            </div>
                        </div>
                    </div>
                    <section class="mt10 white-content">
                        <div class="vm-brief-content desc sticky-box">
                            <div class="tab-all custom-theme-show cl-big-img no_brief">
                               {{introduction}}
                            </div>
                        </div>
                    </section>
                    <div class="flex-box flex-justify-content-space-between flex-align-items-end flex-grow-1 mt10">
                        <div class="flex-box flex-align-items-center">
                            <a class="vm-btn vm-btn-follow mr10">关注</a>
                            <a class="vm-btn vm-btn-publish played-btn signplayed mr10" data-status="1"
                               url_slug="God_of_War_IV">
                                <i class="cowicon cow-toplay mr5"></i>想玩</a>
                            <a class="vm-btn vm-btn-publish played-btn signplayed" url_slug="God_of_War_IV">
                                <i class="cowicon cow-star mr5"></i>玩过</a>
                        </div>
                    </div>
                    <section class="vm-swiper-pic cl-item-bg mt20 only_img_list_status">
                        <div class="swiper-container gallery-top swiper-container-horizontal">
                            <div class="swiper-wrapper">
                                <div class="layui-carousel" id="test10">
                                    <div carousel-item="">
                                            <div v-for="url in picturesUrls"><img :src=url style="width: 100%;height: 100%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <p class="line3"></p>
            <div class="vm-paging flex-box flex-justify-content-end">
                <div class="inline-flex-box">
                    <span class="page-text mr10">跳转至</span>
                    <input class="page-input mr20" placeholder="页码" type="text"></div>
                <a class="chevron-box mr20 disable">
                    <i class="fs10 fa fa-chevron-left mr5"></i>上一页</a>
                <div class="page-box">1 / 390</div>
                <a class="chevron-box ml20">下一页
                    <i class="fs10 fa fa-chevron-right ml5"></i></a>
            </div>
            <a class="white5 gl-hover-text absolute search-game-door" target="_blank"
               href="/search/game?tag_id=&amp;game_url_slug=&amp;per_page=10&amp;page=1&amp;type=10&amp;q=&amp;platform_support_id=&amp;sort_type=desc&amp;publish_year_range=&amp;is_free=0&amp;is_discount=0&amp;is_chinese=0&amp;play_type=">查看全部游戏</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="../../res/layui/layui.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<script>
    //Demo
    layui.use('form', function () {
        var form = layui.form;
        //监听提交
        form.on('submit(formDemo)', function (data) {
            //layer.msg(JSON.stringify(data.field));
            //return false;
        });
    });
    layui.use('upload', function () {
        var $ = layui.jquery
                , upload = layui.upload;
        //普通图片上传
        var uploadInst = upload.render({
            elem: '#test1'
            , url: '/post/upload'
            , before: function (obj) {
                //预读本地文件示例，不支持ie8
                obj.preview(function (index, file, result) {
                    $('#demo1').attr('src', result); //图片链接（base64）
                });
            }
            , done: function (res) {
                //如果上传失败
                if (res.code > 0) {
                    return layer.msg('上传失败');
                }
                //上传成功
            }
            , error: function () {
                //演示失败状态，并实现重传
                var demoText = $('#demoText');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function () {
                    uploadInst.upload();
                });
            }
        });
    });
    /*渲染右边游戏详情*/
    const app = new Vue({
        el: "#gameA",
        data: {
            score:10,
            introduction:"i love it",
            name:"game!",
            plas: [],
            elements:[],
            items:[],
            picturesUrls :[]
        },
        created(){
            fetch("/game/card/17" ).then(response => response.json() ).then(
                    json =>{
                        this.plas = json.platforms;
                    }
            )
        },
        methods: {
            inGame: function (event) {
                var target = event.target;
                var gameId = target.getAttribute("data-gameId");
                fetch("/game/card/" + gameId).then(response => response.json() ).then(
                    json =>{
                       this.score = json.score;
                       this.introduction=json.introduction;
                       this.name = json.name;
                       this.plas = json.platforms;
                       this.elements = json.elements;
                       this.picturesUrls = json.picturesUrl;
                       layui.use(['carousel', 'form'], function(){
                            var carousel = layui.carousel
                                    ,form = layui.form;

                            //图片轮播
                            carousel.render({
                                elem: '#test10'
                                ,width: '430px'
                                ,height: '241px'
                                ,interval: 5000
                            });

                            var $ = layui.$, active = {
                                set: function(othis){
                                    var THIS = 'layui-bg-normal'
                                            ,key = othis.data('key')
                                            ,options = {};
                                    othis.css('background-color', '#5FB878').siblings().removeAttr('style');
                                    options[key] = othis.data('value');
                                    ins3.reload(options);
                                }
                            };

                            //监听开关
                            form.on('switch(autoplay)', function(){
                                ins3.reload({
                                    autoplay: this.checked
                                });
                            });
                            $('.demoSet').on('keyup', function(){
                                var value = this.value
                                        ,options = {};
                                if(!/^\d+$/.test(value)) return;

                                options[this.name] = value;
                                ins3.reload(options);
                            });
                        });

                    }
                )

            }
        }
    })
</script>
</@layout>