/*
+--------------------------------------------------------------------------
|   Mblog [#RELEASE_VERSION#]
|   ========================================
|   Copyright (c) 2014, 2015 mtons. All Rights Reserved
|   http://www.mtons.com
|
+---------------------------------------------------------------------------
*/
package mblog.web.controller.desk.game;


import mblog.core.data.AccountProfile;
import mblog.core.data.Game;
import mblog.core.data.Post;
import mblog.core.data.User;
import mblog.core.persist.entity.GamePo;
import mblog.core.persist.service.GameService;
import mblog.core.persist.service.PostService;
import mblog.core.persist.service.UserService;
import mblog.web.controller.BaseController;
import mblog.web.controller.desk.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Channel 主页
 * @author langhsu
 *
 */
@Controller
@RequestMapping("/game")
public class GameController extends BaseController {

    @Autowired
    private GameService gameService;
    @Autowired
    private UserService userService;
    @Autowired
    private PostService postService;

    /**
     * 发布游戏页
     * */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(ModelMap model){
        return view("/game/add");
    }

    /**
     * 发布游戏
     * */
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String add(Game game, HttpSession session){
        game.setImage((String) session.getAttribute("purl"));
        AccountProfile up = getSubject().getProfile();
        game.setCreator(up.getId());
        gameService.save(game);
        session.removeAttribute("purl");
        return null;
    }

    /**
     * 游戏分页
     * */
    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public String index(@RequestParam(value = "page", defaultValue = "0") Integer page,
                        @RequestParam(value = "size", defaultValue = "10") Integer size,
                        ModelMap modelMap){
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(page, size, sort);
        modelMap.put("games", gameService.paging(pageable));
        modelMap.put("totalPage",gameService.count()/size);
        modelMap.put("page",page);
        return view(Views.GAME_POST_INDEX);
    }

    /**
     * 异步请求游戏的详细信息
     * */
    @ResponseBody
    @RequestMapping(value = "card/{gameId}",method = RequestMethod.GET)
    public  Game card(@PathVariable("gameId") long gameId){
        Game game = gameService.show(gameId);
        return game;
    }


    /**
     * 该游戏的首页
     * */
    @RequestMapping(value = "/{gameId}",method = RequestMethod.GET)
    public String game(@PathVariable("gameId") long gameId,ModelMap modelMap){
        Game game = gameService.show(gameId);
        modelMap.put("game",game);
        User user = userService.get(game.getCreator());
        modelMap.put("creator",user);
        return view("/game/game");
    }

    /**
     * 游戏评论页面
     * */
    @RequestMapping(value = "/review/{gameId}",method = RequestMethod.GET)
    public String review(@PathVariable("gameId") long gameId,ModelMap modelMap){
        Game game = gameService.show(gameId);
        modelMap.put("game",game);
        List<Post> posts = postService.review(gameId,8);
        Map<String,Integer> scores = new HashMap<>(5);
        scores.put("1",0);
        scores.put("2",0);
        scores.put("3",0);
        scores.put("4",0);
        scores.put("5",0);
        double sum = 0;
        for (Post post :posts){
            sum+= post.getSocre();
            switch (post.getSocre()){
                case 1: scores.put("1",scores.get("1")+1);break;
                case 2: scores.put("1",scores.get("1")+1);break;
                case 3: scores.put("2",scores.get("2")+1);break;
                case 4: scores.put("2",scores.get("2")+1);break;
                case 5: scores.put("3",scores.get("3")+1);break;
                case 6: scores.put("3",scores.get("3")+1);break;
                case 7: scores.put("4",scores.get("4")+1);break;
                case 8: scores.put("4",scores.get("4")+1);break;
                case 9: scores.put("5",scores.get("5")+1);break;
                case 10: scores.put("5",scores.get("5")+1);break;
                default:break;
            }
        }
        double ave = sum/posts.size();
        Map<String,String> percentage = new HashMap<>(5);
        /**
         * 截取2位小数
         * */
        percentage.put("1", String .format("%.1f",(scores.get("1")*100.0/posts.size())));
        percentage.put("2", String .format("%.1f",(scores.get("2")*100.0/posts.size())));
        percentage.put("3", String .format("%.1f",(scores.get("3")*100.0/posts.size())));
        percentage.put("4", String .format("%.1f",(scores.get("4")*100.0/posts.size())));
        percentage.put("5", String .format("%.1f",(scores.get("5")*100.0/posts.size())));
        modelMap.put("ave",ave);
        modelMap.put("percentage",percentage);
        modelMap.put("reviews",posts);
        return view("/game/review");
    }

    /**
     * 游戏添加评论页面
     * */
    @RequestMapping(value = "/review/add/{gameId}",method = RequestMethod.GET)
    public String addReview(@PathVariable("gameId") long gameId,ModelMap map){
        map.put("gameId",gameId);
        return view("/game/addReview");
    }

    /**
     * 游戏添加评论
     * */
    @ResponseBody
    @RequestMapping(value = "/review/addReview",method = RequestMethod.POST)
    public ModelMap addReview(Post post,long gameId, int score,ModelMap modelMap){
        AccountProfile profile = getSubject().getProfile();
        post.setAuthorId(profile.getId());
        post.setStatus(8);
        post.setSocre(score);
        post.setGameId(gameId);
        postService.post(post);
        modelMap.put("status","success");
        modelMap.put("msg","提交成功");
        return modelMap;
    }

    /**
     * 搜索游戏名
     * */
    @ResponseBody
    @RequestMapping(value = "/search")
    public List<GamePo> search(String word){
        return gameService.games(word);
    }

}
