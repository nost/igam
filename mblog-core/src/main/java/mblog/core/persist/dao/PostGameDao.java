package mblog.core.persist.dao;

import mblog.core.persist.entity.PostElementPo;
import mblog.core.persist.entity.PostGamePo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PostGameDao extends JpaRepository<PostGamePo,Long>,JpaSpecificationExecutor<PostGameDao>{

}
