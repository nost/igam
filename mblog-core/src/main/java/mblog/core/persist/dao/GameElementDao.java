package mblog.core.persist.dao;

import mblog.core.persist.entity.ElementPo;
import mblog.core.persist.entity.GameElementPo;
import mblog.core.persist.entity.GamePo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface GameElementDao extends JpaRepository<GameElementPo,Long>,JpaSpecificationExecutor<GameElementPo>{
    /**
     * 该游戏的元素
     * */
    List<GameElementPo> findAllByGamePo(GamePo gamePo);
}
