package mblog.core.persist.entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;


/**
 * 游戏标签关联
 * @author syj
 */
@Entity
@Table(name = "mto_game_element")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class GameElementPo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "game_id",nullable = false)
    private GamePo gamePo;

    @ManyToOne
    @JoinColumn(name = "element_id",nullable = false)
    private ElementPo elementPo;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public GamePo getGamePo() {
        return gamePo;
    }

    public void setGamePo(GamePo gamePo) {
        this.gamePo = gamePo;
    }

    public ElementPo getElementPo() {
        return elementPo;
    }

    public void setElementPo(ElementPo elementPo) {
        this.elementPo = elementPo;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
