<#include "/default/utils/ui.ftl"/>
<link rel="stylesheet" type="text/css" href="../../my/css/layui.css">
<link rel="stylesheet" href="../../res/layui/css/layui.css">
<link rel="stylesheet" href="../../res/css/global.css">
<link rel="stylesheet" type="text/css" href="../../my/css/base.css">
<link rel="stylesheet" type="text/css" href="../../my/css/common.css">
<link rel="stylesheet" type="text/css" href="../../my/css/custom-theme.css">
<link rel="stylesheet" type="text/css" href="../../my/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="../../my/css/gameV2.css">
<link rel="stylesheet" type="text/css" href="../../my/css/navV3.css">
<link rel="stylesheet" type="text/css" href="../../my/css/pagespeed.co.wREF_Lb4uo.css">
<link rel="stylesheet" type="text/css" href="../../my/css/swiper-3.4.2.min.css">
<link rel="stylesheet" type="text/css" href="../../my/css/syj.css">

<@layout "评价">


<div class="game-detail-head wrap-outer-padding " style="padding-left:0px">
    <div class="vm-game-detail-header border-radius-top cl-item-bg-9 head_block_display_type_0 show_head_pic_1 show_discover_0 no_head_pic">
        <div class="cl-wrapper flex-box p20 relative border-box" style="min-height: 150px">
            <div class="game-info-box flex-box flex-item-1 flex-direction-column">
                <h1 class="fwb fs32 white lh36 pr20">
                ${game.name}
                </h1>
                <div class="white-content mt10">
                    反正就是很有趣的挂机游戏就对了
                </div>
                <div class="flex-box flex-justify-content-space-between flex-align-items-end flex-grow-1 mt10">
                    <div class="flex-box flex-align-items-center mr10">
                        <a class="vm-btn vm-btn-follow mr10">
                            关注
                        </a>
                        <a class="vm-btn vm-btn-publish played-btn signplayed" data-status="1"
                           url_slug="Soul_Smith_of_the_Kingdom">
                            <i class="cowicon cow-toplay mr5">
                            </i>
                            想玩
                        </a>
                        <a class="vm-btn vm-btn-publish played-btn signplayed" url_slug="Soul_Smith_of_the_Kingdom">
                            <i class="cowicon cow-star mr5">
                            </i>
                            玩过
                        </a>
                    </div>
                </div>
            </div>
            <div class="played-score-box">
                <div class="white3">
                    igame评分
                </div>
                <div class="fs42 yellow fontFace-Avenir lhi">
                ${game.score}
                    <span class="mr5 ml5 white3 fs12">
                        /
                    </span>
                    <span class="white3 fs12">
                        10
                    </span>
                </div>
            </div>
        </div>
        <div class="cl-wrapper">
            <p class="line6">
            </p>
        </div>
    </div>
    <div id="sticky-wrapper" class="sticky-wrapper" style="height: 49px;">
        <div class="vm-game-detail-nav border-radius-bottom cl-item-bg-9">
            <div class="cl-wrapper flex-box flex-center text-center relative">
                <a class="mr40 nav-href-item on" href="/game/Soul_Smith_of_the_Kingdom">
                    主页
                </a>
                <a class="mr40 nav-href-item" href="/game/Soul_Smith_of_the_Kingdom/review">
                    评价体验
                    <#if (game.evaluation > 0)>
                        <span>
                        (${game.evaluation})
                    </span>
                    </#if>
                </a>
                <a class="mr40 nav-href-item" href="/game/Soul_Smith_of_the_Kingdom/image">
                    图片
                    <#if (game.picturesUrl?size > 0)>
                        <span>
                        (${game.picturesUrl?size})
                    </span>
                    </#if>
                </a>
                <a class="mr40 nav-href-item" href="/game/Soul_Smith_of_the_Kingdom/article">
                    相关文章
                    <#if (game.post > 0)>
                        <span>
                        (${game.post})
                    </span>
                    </#if>
                </a>
                <a class="nav-href-item" href="/game/Soul_Smith_of_the_Kingdom/guide">
                    攻略指南
                    <#if (game.strategie > 0)>
                        <span>
                        (${game.strategie})
                    </span>
                    </#if>
                </a>
                <div class="add-content-box" style="display: none;">
                    <i class="fa fa-plus mr5">
                    </i>
                    添加内容
                    <ul class="add-list">
                        <li>
                            <a class="signplayed gl-hover-text-white" url_slug="Soul_Smith_of_the_Kingdom">
                                发表评价
                            </a>
                        </li>
                        <li>
                            <a class="open-question-box-btn gl-hover-text-white" url-slug="Soul_Smith_of_the_Kingdom"
                               game-title="王国的灵魂锻造师 Soul Smith of the Kingdom">
                                提出问题
                            </a>
                        </li>
                        <li class="line">
                        </li>
                        <li>
                            <a class="gl-hover-text-white" rel="nofollow" target="_blank" href="/article/new?url_slugs=Soul_Smith_of_the_Kingdom">
                                撰写文章
                            </a>
                        </li>
                        <li>
                            <a class="gl-hover-text-white" rel="nofollow" target="_blank" href="/article/new?url_slugs=Soul_Smith_of_the_Kingdom&amp;id=237">
                                撰写指南
                            </a>
                        </li>
                        <li class="line">
                        </li>
                        <li>
                            <a class="gl-hover-text-white" rel="nofollow" target="_blank" href="/game/Soul_Smith_of_the_Kingdom/image/new">
                                上传图片
                            </a>
                        </li>
                        <li>
                            <a class="gl-hover-text-white" rel="nofollow" target="_blank" href="/game/Soul_Smith_of_the_Kingdom/video/new">
                                添加视频
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="router-tab mt20">
    <div class="vmpage-game-detail-review cl-wrapper flex-box" infinite-scroll-disabled="busy"
         infinite-scroll-distance="400" articles="" photos="[object Object],[object Object],[object Object],[object Object],[object Object]"
         game_contributors="[object Object],[object Object]" related_questions=""
         videos="" guides="[object Object]" game_developers="[object Object]" feature_videos=""
         feature_images="[object Object],[object Object],[object Object],[object Object],[object Object]"
         related_games="[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object]"
         indexed_collections="" pro_users="">
        <div class="cl-wrapper-left">
            <section class="relative">
                <div class="cl-item-bg-9 p20 flex-box flex-justify-content-space-between pb0">
                    <div class="flex-box">
                        <div class="tab-status fs20 pb10 on">
                            3 人玩过
                        </div>
                        <div class="tab-status ml40 fs20 pb10">
                            1 人想玩
                        </div>
                    </div>
                    <a class="layui-btn layui-btn-sm vm-btn-publish w110 signplayed newReview"  data-gameId="${game.id}">
                        撰写新评价
                    </a>
                </div>
                <div class="p_0_20 cl-item-bg">
                    <p class="line6">
                    </p>
                </div>
                <div class="flex-box cl-item-bg-9 p20">
                    <section class="vm-star-bar flex-box flex-item-6 mr40">
                        <div class="mr10">
                            <p class="fs48 fontFace-Avenir white lh1_4">
                                ${ave}
                            </p>
                            <p class="white5">
                                平均得分
                            </p>
                        </div>
                        <div class="flex-item-1">
                            <div class="flex-box yellow flex-align-items-center lh1_4" data-role="tips"
                                 data-tips-content="0.0%" data-tips-layer-index="6">
                    <span class="white">
                      5
                    </span>
                                <i class="fa fa-star m_0_5 fs">
                                </i>
                                <div class="star-bar-bg flex-item-1">
                                        <div class="layui-progress " >
                                            <div class="layui-progress-bar layui-bg-orange" lay-percent="${percentage["5"]}%" ></div>
                                        </div>
                                </div>
                            </div>
                            <div class="flex-box yellow flex-align-items-center lh1_4" data-role="tips"
                                 data-tips-content="0.0%" data-tips-layer-index="7">
                    <span class="white">
                      4
                    </span>
                                <i class="fa fa-star m_0_5 fs">
                                </i>
                                <div class="star-bar-bg flex-item-1">
                                    <div class="layui-progress " >
                                    <div class="layui-progress-bar layui-bg-orange" lay-percent="${percentage["4"]}%"></div>
                                </div>
                                </div>
                            </div>
                            <div class="flex-box yellow flex-align-items-center lh1_4" data-role="tips"
                                 data-tips-content="66.7%" data-tips-layer-index="8">
                    <span class="white">
                      3
                    </span>
                                <i class="fa fa-star m_0_5 fs">
                                </i>
                                <div class="star-bar-bg flex-item-1">
                                    <div class="layui-progress " >
                                        <div class="layui-progress-bar layui-bg-orange" lay-percent="${percentage["3"]}%"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-box yellow flex-align-items-center lh1_4" data-role="tips"
                                 data-tips-content="33.3%" data-tips-layer-index="9">
                    <span class="white">
                      2
                    </span>
                                <i class="fa fa-star m_0_5 fs">
                                </i>
                                <div class="star-bar-bg flex-item-1">
                                    <div class="layui-progress " >
                                        <div class="layui-progress-bar layui-bg-orange" lay-percent="${percentage["2"]}%"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-box yellow flex-align-items-center lh1_4" data-role="tips"
                                 data-tips-content="0.0%" data-tips-layer-index="10">
                    <span class="white">
                      1
                    </span>
                                <i class="fa fa-star m_0_5 fs">
                                </i>
                                <div class="star-bar-bg flex-item-1">
                                    <div class="layui-progress " >
                                        <div class="layui-progress-bar layui-bg-orange" lay-percent="${percentage["1"]}%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="vm-played-time-bar flex-box flex-item-7">
                        <div style="display: none" class="nowrap mr20">
                            <p class="fs48 fontFace-Avenir white lh1_4">
                                25.0
                            </p>
                            <p class="white5">
                                平均时长（h）
                            </p>
                        </div>
                        <div style="display: none" class="flex-item-1">
                            <div class="flex-box flex-align-items-center lh1_4" data-role="tips" data-tips-content="0.0%"
                                 data-tips-layer-index="5">
                    <span class="white text-right mr5" style="width:56px">
                      &lt; 2
                    </span>
                                <div class="played-time-bar-bg flex-item-1">
                                    <div class="played-time-bar" style="width: 0%;">
                                    </div>
                                </div>
                            </div>
                            <div class="flex-box flex-align-items-center lh1_4" data-role="tips" data-tips-content="33.3%"
                                 data-tips-layer-index="4">
                    <span class="white text-right mr5" style="width:56px">
                      2 ~ 10
                    </span>
                                <div class="played-time-bar-bg flex-item-1">
                                    <div class="played-time-bar" style="width: 33.3333%;">
                                    </div>
                                </div>
                            </div>
                            <div class="flex-box flex-align-items-center lh1_4" data-role="tips" data-tips-content="33.3%"
                                 data-tips-layer-index="3">
                    <span class="white text-right mr5" style="width:56px">
                      10 ~ 30
                    </span>
                                <div class="played-time-bar-bg flex-item-1">
                                    <div class="played-time-bar" style="width: 33.3333%;">
                                    </div>
                                </div>
                            </div>
                            <div class="flex-box flex-align-items-center lh1_4" data-role="tips" data-tips-content="33.3%">
                    <span class="white text-right mr5" style="width:56px">
                      30 ~ 100
                    </span>
                                <div class="played-time-bar-bg flex-item-1">
                                    <div class="played-time-bar" style="width: 33.3333%;">
                                    </div>
                                </div>
                            </div>
                            <div class="flex-box flex-align-items-center lh1_4" data-role="tips" data-tips-content="0.0%">
                    <span class="white text-right mr5" style="width:56px">
                      &gt;= 100
                    </span>
                                <div class="played-time-bar-bg flex-item-1">
                                    <div class="played-time-bar" style="width: 0%;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="p20 cl-item-bg-9" style="padding-bottom:0">
                    <section class="vm-select layui-form" style="color: #0C0C0C;margin-bottom: 60px">
                        <select name="city" lay-verify=""  lay-search >
                            <option value="010">最多喜欢</option>
                            <option value="021">最新发布</option>
                            <option value="0571">最多评论</option>
                        </select>
                    </section>
                </div>
                <div>
                    <#list reviews as review>
                    <div class="vm-edit-card-review cl-card cl-item-bg-9 layout-2">
                        <section class="flex-box flex-direction-column h100">
                            <div class="tit mb10 flex-box">
                                <div class="flex-item-1">
                                    <div class="flex-box">
                                        <a class="mr10" target="_blank" data-role="tip-window" data-target=".user-card"
                                           data-tpl="user-card-tpl-2" style="display: block" href="/people/kindred"
                                           data-user-slug="kindred">
                                            <img class="vm-img-lazy circle wh20 cl-vm-lazyload-img syj-avatar" src="${review.author.avatar}"
                                                 lazy="loaded">
                                        </a>
                                        <a class="red mr10" target="_blank" data-role="tip-window" data-target=".user-card"
                                           data-tpl="user-card-tpl-2" href="/people/kindred" data-user-slug="kindred">
                                            ${review.author.name}
                                        </a>
                                        评价
                                    </div>
                                    <div class="flex-box flex-align-items-center">
                        <span>
                          玩过 50 小时
                        </span>
                        <span class="vm-star">
                          <i class="fa fa-star"></i>

                            <#if review.socre gt 2>
                          <i class="fa fa-star"></i>
                            <#else>
                                <i class="fa fa-star-o"></i>
                            </#if>

                            <#if review.socre gt 4>
                                <i class="fa fa-star"></i>
                            <#else>
                                <i class="fa fa-star-o"></i>
                            </#if>

                            <#if review.socre gt 6>
                                <i class="fa fa-star"></i>
                            <#else>
                                <i class="fa fa-star-o"></i>
                            </#if>

                            <#if review.socre gt 8>
                                <i class="fa fa-star"></i>
                            <#else>
                                <i class="fa fa-star-o"></i>
                            </#if>
                        </span>
                                <div>
                                    <a class="action-time pointer gl-hover-text" target="_blank" data-role="tips-time-human"
                                       href="/game/Soul_Smith_of_the_Kingdom/review/3158623" data-tips-content-publish="2018-05-02 12:44:15"
                                       data-tips-content-update="2018-05-02 20:24:11">
                                        2018-05-02
                                    </a>
                                </div>
                            </div>
                            <div class="flex-box flex-item-2">
                                <div class="content-box inline-flex-box flex-direction-column flex-wrap">
                                    <div class="desc sticky-box">
                                        <div class="tab-brief flex-box" style="display: block !important;">
                                            <div class="tab-brief-text flex-item-1 syj-comment" style="float: left">
                                                <div class="cont" style="height: 92px;width: 600px;overflow: hidden">
                                                ${review.content}
                                                </div>
                                                <span class="show-all-btn" data-postId="${review.id}" >显示全部</span>
                                        </div>
                                            <div class="tab-brief-img bg-img cl-vm-lazyload-img" lazy="loaded" style="background-image: url(&quot;https://pic1.cdncl.net/user/kindred/common_pic/0a4fd4b9faf1268f53223699da45bfe8.jpg?imageView2/1/w/360/h/200&quot;);height: 100px;width: 180px;height: 100px;margin-left: 420px; display: none"></div>
                                        <div class="flex-box flex-align-items-center mt20 flex-align-self-end footer-info-box"
                                             data-sticky-column="" >
                                            <a class="vm-btn vm-btn-vote">
                                                <i class="icon cowicon cow-praise">
                                                </i>
                                                喜欢
                                            </a>
                                            <div class="flex-item-1 flex-box flex-align-items-center">
                                                <div class="flex-box">
                                                    <a class="ml white-5 pointer gl-hover-text-white">
                                                        ${review.favors}人喜欢
                                                    </a>
                                                    <a class="ml white-5 pointer gl-hover-text-white ml10" target="_blank"
                                                       href="/game/Soul_Smith_of_the_Kingdom/review/3158623#comment-anchor">
                                                        ${review.comments}条评论
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    </#list>
                </div>
                <div class="loader-inner pacman search-pacman hide">
                    <div>
                    </div>
                    <div>
                    </div>
                    <div>
                    </div>
                    <div>
                    </div>
                    <div>
                    </div>
                </div>
                <div class="cl-card cl-item-bg-9 text-center">
                    <div class="white5">
                        暂时没有更多评价
                    </div>
                </div>
            </section>
        </div>
        <div class="cl-wrapper-right">
        </div>
    </div>
</div>

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="../../res/layui/layui.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    layui.use('form', function(){
        var form = layui.form;
    });
    //注意进度条依赖 element 模块，否则无法进行正常渲染和功能性操作
    layui.use('element', function(){
        var element = layui.element;
    });
    layui.use('layer', function(){
        var layer = layui.layer;
        $(".layui-progress-bar").mouseenter(function () {
            var percent = $(this).attr("lay-percent");
            layer.tips(percent, '.layui-progress-bar',{
                tips: [1]
            });
        });
        
        $(".show-all-btn").click(function () {
            var url = $(this).attr("data-postId")
            layer.open({
                type: 2,
                title: '评价',
                shadeClose: true,
                shade: 0.8,
                area: ['380px', '90%'],
                content: '/view/'+url,
                offset: ['60px', '550px']
            })
        })

        $(".newReview").click(function () {
            var url = $(this).attr("data-gameId")
            layer.open({
                type: 2,
                title: '新评价',
                shadeClose: true,
                shade: 0.8,
                area: ['1050px', '90%'],
                content: '/game/review/add/'+url,
                offset: ['60px', '200px']
            })
        })
    });
</script>
<script>
    $(document).ready(function () {
        $(".syj-comment").each(function (index,card) {
            var image = $(this).find("img");
            image.each(function (index) {
                if (index == 0){
                    var url = "../" + $(this).attr("src");
                    $(this).parents(".cont").css("width","400px");
                    $(this).parents(".syj-comment").next(".bg-img").css("display","block");
                    $(this).parents(".syj-comment").next(".bg-img").css("background-image","url("+url+")");
                }
                $(this).css("display","none");
            })
        })
        $(".show-all-btn").click(function () {

        })
    });
</script>

</@layout>