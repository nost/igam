package mblog.core.persist.dao;

import mblog.core.persist.entity.FollowElementPo;
import mblog.core.persist.entity.FollowGamePo;
import mblog.core.persist.entity.GamePo;
import mblog.core.persist.entity.UserPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface FollowGameDao extends JpaRepository<FollowGamePo,Long>,JpaSpecificationExecutor<FollowGamePo> {
    List<FollowGamePo> findByUser(UserPO userPO);
    List<FollowGamePo> findByUserAndFollowGame(UserPO userPO,GamePo gamePo);
    void deleteByUserAndFollowGame(UserPO userPO,GamePo gamePo);
}
