<#include "/default/utils/ui.ftl"/>
<@layout "创建游戏">
<link rel="stylesheet" href="../../res/layui/css/layui.css">
<link rel="stylesheet" href="../../res/css/global.css">
<div class="layui-container fly-marginTop">
    <div class="fly-panel" pad20 style="padding-top: 5px;">
        <!--<div class="fly-none">没有权限</div>-->
        <div class="layui-form layui-form-pane">
            <div class="layui-tab layui-tab-brief" lay-filter="user">
                <ul class="layui-tab-title">
                    <li class="layui-this">创建游戏<!-- 编辑帖子 --></li>
                </ul>
                <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                    <div class="layui-tab-item layui-show">
                        <form action="/game/add" method="post">
                            <div class="layui-row layui-col-space15 layui-form-item">
                                <div class="layui-col-md9">
                                    <label for="L_title" class="layui-form-label">游戏名</label>
                                    <div class="layui-input-block">
                                        <input type="text" id="L_title" name="title" required lay-verify="required" autocomplete="off" class="layui-input">
                                        <!-- <input type="hidden" name="id" value="{{d.edit.id}}"> -->
                                    </div>
                                </div>
                            </div>
                            <div class="layui-upload">
                                <button type="button" class="layui-btn" id="test1">上传游戏封面</button>
                                <div class="layui-upload-list">
                                    <img class="layui-upload-img" id="demo1" style="width: 300px;height: 200px;">
                                    <p id="demoText"></p>
                                </div>
                            </div>

                            <div class="layui-form-item layui-form-text">
                                <div class="layui-input-block">
                                    <textarea id="L_content" name="content" required lay-verify="required" placeholder="游戏简介" class="layui-textarea fly-editor" style="height: 260px;"></textarea>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">发行时间</label>
                                    <div class="layui-input-inline" style="width: 190px;">
                                        <select name="year">
                                            <option   value="">待定  </option>
                                            <option   value="2020">2020年</option>
                                            <option   value="2019">2019年</option>
                                            <option   value="2018">2018年</option>
                                            <option   value="2017">2017年</option>
                                            <option   value="2016">2016年</option>
                                            <option   value="2015">2015年</option>
                                            <option   value="2014">2014年</option>
                                            <option   value="2013">2013年</option>
                                            <option   value="2012">2012年</option>
                                            <option   value="2011">2011年</option>
                                            <option   value="2010">2010年</option>
                                            <option   value="2009">2009年</option>
                                            <option   value="2008">2008年</option>
                                            <option   value="2007">2007年</option>
                                            <option   value="2006">2006年</option>
                                            <option   value="2005">2005年</option>
                                            <option   value="2004">2004年</option>
                                            <option   value="2003">2003年</option>
                                            <option   value="2002">2002年</option>
                                            <option   value="2001">2001年</option>
                                            <option   value="2000">2000年</option>
                                            <option   value="1999">1999年</option>
                                            <option   value="1998">1998年</option>
                                            <option   value="1997">1997年</option>
                                            <option   value="1996">1996年</option>
                                            <option   value="1995">1995年</option>
                                            <option   value="1994">1994年</option>
                                            <option   value="1993">1993年</option>
                                            <option   value="1992">1992年</option>
                                            <option   value="1991">1991年</option>
                                            <option   value="1990">1990年</option>
                                            <option   value="1989">1989年</option>
                                            <option   value="1988">1988年</option>
                                            <option   value="1987">1987年</option>
                                            <option   value="1986">1986年</option>
                                            <option   value="1985">1985年</option>
                                            <option   value="1984">1984年</option>
                                            <option   value="1983">1983年</option>
                                            <option   value="1982">1982年</option>
                                            <option   value="1981">1981年</option>
                                            <option   value="1980">1980年</option>
                                            <option   value="1979">1979年</option>
                                            <option   value="1978">1978年</option>
                                            <option   value="1977">1977年</option>
                                            <option   value="1976">1976年</option>
                                            <option   value="1975">1975年</option>
                                            <option   value="1974">1974年</option>
                                            <option   value="1973">1973年</option>
                                            <option   value="1972">1972年</option>
                                            <option   value="1971">1971年</option>
                                            <option   value="1970">1970年</option>
                                            <option   value="1969">1969年</option>
                                            <option   value="1968">1968年</option>
                                            <option   value="1967">1967年</option>
                                            <option   value="1966">1966年</option>
                                            <option   value="1965">1965年</option>
                                            <option   value="1964">1964年</option>
                                            <option   value="1963">1963年</option>
                                            <option   value="1962">1962年</option>
                                            <option   value="1961">1961年</option>
                                            <option   value="1960">1960年</option>
                                            <option   value="1959">1959年</option>
                                            <option   value="1958">1958年</option>
                                            <option   value="1957">1957年</option>
                                            <option   value="1956">1956年</option>
                                            <option   value="1955">1955年</option>
                                            <option   value="1954">1954年</option>
                                            <option   value="1953">1953年</option>
                                            <option   value="1952">1952年</option>
                                            <option   value="1951">1951年</option>
                                            <option   value="1950">1950年</option>
                                            <option   value="1949">1949年</option>
                                            <option   value="1948">1948年</option>
                                            <option   value="1947">1947年</option>
                                        </select>
                                    </div>
                                    <div class="layui-input-inline" style="width: 190px;">
                                        <select name="month">
                                            <option  value="">待定</option>
                                            <option  value="1">1月</option>
                                            <option  value="2">2月</option>
                                            <option  value="3">3月</option>
                                            <option  value="4">4月</option>
                                            <option  value="5">5月</option>
                                            <option  value="6">6月</option>
                                            <option  value="7">7月</option>
                                            <option  value="8">8月</option>
                                            <option  value="9">9月</option>
                                            <option  value="10">10月</option>
                                            <option  value="11">11月</option>
                                            <option  value="12">12月</option>
                                        </select>
                                    </div>
                                    <div class="layui-input-inline" style="width: 190px;">
                                        <select name="day">
                                            <option  value="">待定</option>
                                            <option  value="1">1日</option>
                                            <option  value="2">2日</option>
                                            <option  value="3">3日</option>
                                            <option  value="4">4日</option>
                                            <option  value="5">5日</option>
                                            <option  value="6">6日</option>
                                            <option  value="7">7日</option>
                                            <option  value="8">8日</option>
                                            <option  value="9">9日</option>
                                            <option  value="10">10日</option>
                                            <option  value="11">11日</option>
                                            <option  value="12">12日</option>
                                            <option  value="13">13日</option>
                                            <option  value="14">14日</option>
                                            <option  value="15">15日</option>
                                            <option  value="16">16日</option>
                                            <option  value="17">17日</option>
                                            <option  value="18">18日</option>
                                            <option  value="19">19日</option>
                                            <option  value="20">20日</option>
                                            <option  value="21">21日</option>
                                            <option  value="22">22日</option>
                                            <option  value="23">23日</option>
                                            <option  value="24">24日</option>
                                            <option  value="25">25日</option>
                                            <option  value="26">26日</option>
                                            <option  value="27">27日</option>
                                            <option  value="28">28日</option>
                                            <option  value="29">29日</option>
                                            <option  value="30">30日</option>
                                            <option  value="31">31日</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label" style="width: 150px;">游戏支持平台</label>
                                <div class="layui-input-block">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="6" title="Windows	">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="7" title="iOS		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="8" title="Android	">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="9" title="PS  		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="10" title="PS2		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="11" title="PS3		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="12" title="PS4		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="13" title="PSP		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="14" title="PSV		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="15" title="Xbox		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="16" title="Xbox 360	">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="17" title="Xbox One	">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="18" title="FC		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="19" title="SFC		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="20" title="N64		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="21" title="NGPC 		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="22" title="Wii		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="23" title="Wii U		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="24" title="Nintendo Switch"><br>
                                    <input type="checkbox" lay-skin="primary" name="platform" value="25" title="Virtual Boy"    >
                                    <input type="checkbox" lay-skin="primary" name="platform" value="26" title="GB 		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="27" title="GBC		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="28" title="GBA		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="29" title="NDS		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="30" title="3DS		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="31" title="SMS		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="32" title="MD		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="33" title="Sega CD	">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="34" title="SS 		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="35" title="DC		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="36" title="Game Gear	">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="37" title="WSC		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="38" title="NGPC		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="39" title="PC Engine	">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="40" title="PC-FX		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="41" title="Neo Geo CD">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="42" title="街机		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="43" title="桌面游戏	">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="44" title="Mac		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="45" title="Linux		">
                                    <input type="checkbox" lay-skin="primary" name="platform" value="46" title="DOS		">
                                </div>
                                <div class="layui-form-item">
                                    <button class="layui-btn" lay-filter="*" lay-submit>立即发布</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../../res/layui/layui.js"></script>

<script>
    //Demo
    layui.use('form', function(){
        var form = layui.form;

        //监听提交
        form.on('submit(formDemo)', function(data){
            //layer.msg(JSON.stringify(data.field));
            //return false;
        });
    });

    layui.use('upload', function(){
        var $ = layui.jquery
                ,upload = layui.upload;

        //普通图片上传
        var uploadInst = upload.render({
            elem: '#test1'
            ,url: '/post/upload'
            ,before: function(obj){
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){
                    $('#demo1').attr('src', result); //图片链接（base64）
                });
            }
            ,done: function(res){
                //如果上传失败
                if(res.code > 0){
                    return layer.msg('上传失败');
                }
                //上传成功
            }
            ,error: function(){
                //演示失败状态，并实现重传
                var demoText = $('#demoText');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function(){
                    uploadInst.upload();
                });
            }
        });


    });

</script>
</@layout>