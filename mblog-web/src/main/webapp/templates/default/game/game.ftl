<#include "/default/utils/ui.ftl"/>
<@layout "游戏">
<link rel="stylesheet" type="text/css" href="../my/css/layui.css">
<link rel="stylesheet" href="../../res/layui/css/layui.css">
<link rel="stylesheet" href="../../res/css/global.css">
<link rel="stylesheet" type="text/css" href="../my/css/base.css">
<link rel="stylesheet" type="text/css" href="../my/css/common.css">
<link rel="stylesheet" type="text/css" href="../my/css/custom-theme.css">
<link rel="stylesheet" type="text/css" href="../my/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="../my/css/gameV2.css">
<link rel="stylesheet" type="text/css" href="../my/css/navV3.css">
<link rel="stylesheet" type="text/css" href="../my/css/pagespeed.co.wREF_Lb4uo.css">
<link rel="stylesheet" type="text/css" href="../my/css/swiper-3.4.2.min.css">
<link rel="stylesheet" type="text/css" href="../my/css/syj.css">
<div class="game-detail-head wrap-outer-padding " style="padding-left:0px">
    <div class="vm-game-detail-header border-radius-top cl-item-bg-9 head_block_display_type_0 show_head_pic_1 show_discover_0 no_head_pic">
        <div class="cl-wrapper flex-box p20 relative border-box" style="min-height: 150px">
            <div class="game-info-box flex-box flex-item-1 flex-direction-column">
                <h1 class="fwb fs32 white lh36 pr20">
                    ${game.name}
                </h1>
                <div class="white-content mt10">
                    反正就是很有趣的挂机游戏就对了
                </div>
                <div class="flex-box flex-justify-content-space-between flex-align-items-end flex-grow-1 mt10">
                    <div class="flex-box flex-align-items-center mr10">
                        <a class="vm-btn vm-btn-follow mr10">
                            关注
                        </a>
                        <a class="vm-btn vm-btn-publish played-btn signplayed" data-status="1"
                           url_slug="Soul_Smith_of_the_Kingdom">
                            <i class="cowicon cow-toplay mr5">
                            </i>
                            想玩
                        </a>
                        <a class="vm-btn vm-btn-publish played-btn signplayed" url_slug="Soul_Smith_of_the_Kingdom">
                            <i class="cowicon cow-star mr5">
                            </i>
                            玩过
                        </a>
                    </div>
                </div>
            </div>
            <div class="played-score-box">
                <div class="white3">
                    igame评分
                </div>
                <div class="fs42 yellow fontFace-Avenir lhi">
                   ${game.score}
                    <span class="mr5 ml5 white3 fs12">
                        /
                    </span>
                    <span class="white3 fs12">
                        10
                    </span>
                </div>
            </div>
        </div>
        <div class="cl-wrapper">
            <p class="line6">
            </p>
        </div>
    </div>
    <div id="sticky-wrapper" class="sticky-wrapper" style="height: 49px;">
        <div class="vm-game-detail-nav border-radius-bottom cl-item-bg-9">
            <div class="cl-wrapper flex-box flex-center text-center relative">
                <a class="mr40 nav-href-item on" href="/game/Soul_Smith_of_the_Kingdom">
                    主页
                </a>
                <a class="mr40 nav-href-item" href="/game/review/${game.id}">
                    评价体验
                    <#if (game.evaluation > 0)>
                    <span>
                        (${game.evaluation})
                    </span>
                    </#if>
                </a>
                <a class="mr40 nav-href-item" href="/game/Soul_Smith_of_the_Kingdom/image">
                    图片
                    <#if (game.picturesUrl?size > 0)>
                    <span>
                        (${game.picturesUrl?size})
                    </span>
                    </#if>
                </a>
                <a class="mr40 nav-href-item" href="/game/Soul_Smith_of_the_Kingdom/article">
                    相关文章
                    <#if (game.post > 0)>
                        <span>
                        (${game.post})
                    </span>
                    </#if>
                </a>
                <a class="nav-href-item" href="/game/Soul_Smith_of_the_Kingdom/guide">
                    攻略指南
                    <#if (game.strategie > 0)>
                        <span>
                        (${game.strategie})
                    </span>
                    </#if>
                </a>
                <div class="add-content-box" style="display: none;">
                    <i class="fa fa-plus mr5">
                    </i>
                    添加内容
                    <ul class="add-list">
                        <li>
                            <a class="signplayed gl-hover-text-white" url_slug="Soul_Smith_of_the_Kingdom">
                                发表评价
                            </a>
                        </li>
                        <li>
                            <a class="open-question-box-btn gl-hover-text-white" url-slug="Soul_Smith_of_the_Kingdom"
                               game-title="王国的灵魂锻造师 Soul Smith of the Kingdom">
                                提出问题
                            </a>
                        </li>
                        <li class="line">
                        </li>
                        <li>
                            <a class="gl-hover-text-white" rel="nofollow" target="_blank" href="/article/new?url_slugs=Soul_Smith_of_the_Kingdom">
                                撰写文章
                            </a>
                        </li>
                        <li>
                            <a class="gl-hover-text-white" rel="nofollow" target="_blank" href="/article/new?url_slugs=Soul_Smith_of_the_Kingdom&amp;id=237">
                                撰写指南
                            </a>
                        </li>
                        <li class="line">
                        </li>
                        <li>
                            <a class="gl-hover-text-white" rel="nofollow" target="_blank" href="/game/Soul_Smith_of_the_Kingdom/image/new">
                                上传图片
                            </a>
                        </li>
                        <li>
                            <a class="gl-hover-text-white" rel="nofollow" target="_blank" href="/game/Soul_Smith_of_the_Kingdom/video/new">
                                添加视频
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div  class="router-tab mt20 mypadding" >
    <div class="vmpage-game-detail-index cl-wrapper flex-box wrap-outer-padding" style="padding-left: 0">
        <div class="cl-wrapper-left">
            <section class="vm-swiper-pic cl-item-bg mb20 img_obj_list_status">
                <div class="swiper-container gallery-top swiper-container-horizontal">
                    <div class="swiper-wrapper">
                        <div class="layui-carousel" id="test10">
                            <div carousel-item="">
                                <#list game.picturesUrl as url>
                                <div >
                                    <img  src="${url}" style="width: 100%;height: 100%">
                                </div>
                                </#list>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gallery-thumbs-box mt10">
                    <div class="swiper-container gallery-thumbs swiper-container-horizontal swiper-container-free-mode">
                        <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">
                            <#list game.picturesUrl as url>
                            <div class="swiper-slide is-selected swiper-slide-active" style="margin-right: 10px; background-image: url(${url});">
                            </div>
                            </#list>
                        </div>
                        <div class="swiper-scrollbar">
                            <div class="swiper-scrollbar-drag" style="transform: translate3d(0px, 0px, 0px); width: 602.353px;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-next swiper-button-white">
                </div>
                <div class="swiper-button-prev swiper-button-white swiper-button-disabled">
                </div>
            </section>
            <section class="cl-card cl-item-bg-9 white-content mb40">
                <div class="vm-brief-content desc sticky-box">
                    <a class="tab-brief white-content flex-box block">
                        <div class="tab-brief-text flex-item-1">
                            ${game.introduction}
                            <span class="show-all-btn">
                                显示全部
                            </span>
                        </div>
                    </a>
                </div>
            </section>
        </div>
        <div class="cl-wrapper-right">
            <div class="cl-card cl-item-bg-9">
                <div class="publish-time">
                    发行于 ${game.year}年${game.month}月${game.day}日
                </div>
                <dl class="mt10 white flex-box">
                    <dt class="nowrap">
                        元素：
                    </dt>

                    <dd>
                        <#assign x= game.elements?size />
                        <#list game.elements as  element>
                        <a target="_blank" class="white5 gl-hover-text" data-role="tip-tag" data-x="0"
                           data-y="27" href="/element/373" data-id="373">
                            ${elements.name}
                        </a>
                        <#assign x= x-1/>
                        <#if x ==1>
                        <span class="white5" style="margin: 0 5px">
                            /
                        </span>
                        </#if>
                    </#list>
                    </dd>
                </dl>
                <dl class="mt10 white flex-box">
                    <dt class="nowrap">
                        平台：
                    </dt>
                    <dd>
                        <#assign x= game.platforms?size />
                        <#list game.platforms as pla>
                        <a target="_blank" class="white5 gl-hover-text" data-role="tip-tag" data-x="0"
                           data-y="27" href="/element/183" data-id="183">
                            ${pla.name}
                        </a>
                        <#assign x= x-1/>
                        <#if x ==1>
                        <span class="white5" style="margin: 0 5px">
                            /
                        </span>
                        </#if>
                        </#list>
                    </dd>
                </dl>
                <dl class="mt20 white flex-box">
                    <dt class="nowrap">
                        条目贡献者：
                    </dt>
                    <dd class="flex-box">
                        <div class="vm-avatar-small">
                            <a target="_blank" class="avatar-box" data-role="tip-window" data-target=".user-card"
                               data-tpl="user-card-tpl-2" data-left="8" href="/people/jjinheaven" data-user-slug="jjinheaven">
                                <img class="vm-img-lazy circle avatar cl-vm-lazyload-img" src="${creator.avatar}"
                                     lazy="loaded">
                            </a>
                        </div>
                    </dd>
                </dl>
                <dl class="mt20 white flex-box">
                    <dt class="nowrap">
                        其他名称：
                    </dt>
                    <dd class="white5">
                        <h3 class="white5 fwn fs14" style="display: inline">
                            王国的灵魂锻造师
                        </h3>
                        <span class="white5" style="margin: 0 5px">
                            /
                        </span>
                        <h3 class="white5 fwn fs14" style="display: inline">
                            Soul Smith of the Kingdom
                        </h3>
                    </dd>
                </dl>
                <div class="flex-box mt20">
                    <a class="white5 gl-hover-text reply-inbox-btn" data-user-name="Yoge"
                       data-user-slug="yoge" data-content="Yoge 你好，我想申请全站游戏的公共编辑权限，共同建设奶牛关。<br>https://cowlevel.net/game/Soul_Smith_of_the_Kingdom">
                        申请编辑
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="../../res/layui/layui.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>layui.use(['carousel', 'form'], function(){
        var carousel = layui.carousel
                ,form = layui.form;
        //图片轮播
        carousel.render({
            elem: '#test10'
            ,width: '778px'
            ,height: '440px'
            ,interval: 5000
        });

        //事件
        carousel.on('change(test4)', function(res){
            console.log(res)
        });

        var $ = layui.$, active = {
            set: function(othis){
                var THIS = 'layui-bg-normal'
                        ,key = othis.data('key')
                        ,options = {};

                othis.css('background-color', '#5FB878').siblings().removeAttr('style');
                options[key] = othis.data('value');
                ins3.reload(options);
            }
        };

        //监听开关
        form.on('switch(autoplay)', function(){
            ins3.reload({
                autoplay: this.checked
            });
        });

        $('.demoSet').on('keyup', function(){
            var value = this.value
                    ,options = {};
            if(!/^\d+$/.test(value)) return;

            options[this.name] = value;
            ins3.reload(options);
        });

    });
    </script>
    </script>
</@layout>