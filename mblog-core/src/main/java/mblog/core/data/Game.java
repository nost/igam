package mblog.core.data;

import mblog.core.persist.entity.ElementPo;
import mblog.core.persist.entity.GamePo;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class Game extends GamePo implements Serializable {
    /**
     * 相关平台
     * */
    private long[] platform;
    private List<ElementPo> platforms;

    /**
     * 相关元素
     * */
    private long[] element;
    private List<ElementPo> elements;

    /**
     * 评价数
     * */
    private int evaluation;

    /**
     * 图片
     * */
    private List<String> picturesUrl;

    /**
     * 文章数
     * */
    private int post;

    /**
     * 攻略数
     * */
    private int strategie;

    public long[] getPlatform() {
        return platform;
    }

    public void setPlatform(long[] platform) {
        this.platform = platform;
    }

    public List<ElementPo> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<ElementPo> platforms) {
        this.platforms = platforms;
    }

    public long[] getElement() {
        return element;
    }

    public void setElement(long[] element) {
        this.element = element;
    }

    public List<ElementPo> getElements() {
        return elements;
    }

    public void setElements(List<ElementPo> elements) {
        this.elements = elements;
    }

    public int getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(int evaluation) {
        this.evaluation = evaluation;
    }

    public List<String> getPicturesUrl() {
        return picturesUrl;
    }

    public void setPicturesUrl(List<String> picturesUrl) {
        this.picturesUrl = picturesUrl;
    }

    public int getPost() {
        return post;
    }

    public void setPost(int post) {
        this.post = post;
    }

    public int getStrategie() {
        return strategie;
    }

    public void setStrategie(int strategie) {
        this.strategie = strategie;
    }

}
